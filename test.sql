-- MySQL dump 10.13  Distrib 5.6.17, for Win64 (x86_64)
--
-- Host: localhost    Database: ferfran
-- ------------------------------------------------------
-- Server version	5.6.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `caja`
--

DROP TABLE IF EXISTS `caja`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `caja` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Abre` int(11) NOT NULL,
  `Cierra` int(11) DEFAULT '-255',
  `Fechaabre` datetime NOT NULL,
  `Fechacierra` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `caja`
--

LOCK TABLES `caja` WRITE;
/*!40000 ALTER TABLE `caja` DISABLE KEYS */;
INSERT INTO `caja` VALUES (5,15000,3071000,'2015-04-13 00:00:00','2015-04-14 00:00:00'),(16,20000,20000,'2015-04-14 00:00:00','2015-04-14 00:00:00'),(18,20000,2000,'2015-04-14 00:00:00','2015-04-14 23:50:59'),(19,20000,290000,'2015-04-14 00:00:00','2015-04-15 00:09:28'),(20,20000,20000,'2015-04-15 00:00:00','2015-04-15 21:38:51'),(21,20000,20000,'2015-04-15 00:00:00','2015-04-15 21:40:33'),(22,15000,35000,'2015-04-15 00:00:00','2015-04-16 16:28:22'),(23,20000,20000,'2015-04-16 00:00:00','2015-04-16 17:34:04'),(24,20000,20000,'2015-04-16 00:00:00','2015-04-16 17:36:41'),(25,20000,20000,'2015-04-16 00:00:00','2015-04-16 17:37:56'),(26,12550,12550,'2015-04-16 00:00:00','2015-04-16 17:39:33'),(27,20000,20000,'2015-04-16 00:00:00','2015-04-16 18:19:06'),(28,20000,20000,'2015-04-16 00:00:00','2015-04-16 18:21:31'),(29,20000,20000,'2015-04-16 00:00:00','2015-04-16 18:22:20'),(30,20000,20000,'2015-04-16 00:00:00','2015-04-16 18:23:02'),(31,20000,20000,'2015-04-16 00:00:00','2015-04-16 18:26:27'),(32,20000,20000,'2015-04-16 00:00:00','2015-04-16 18:27:14'),(33,20000,20000,'2015-04-16 00:00:00','2015-04-16 18:27:45'),(34,20000,20000,'2015-04-16 00:00:00','2015-04-16 18:34:39'),(35,20000,20000,'2015-04-16 00:00:00','2015-04-16 18:36:37'),(36,200000,200000,'2015-04-16 00:00:00','2015-04-16 18:51:43'),(37,20000,20000,'2015-04-16 00:00:00','2015-04-16 18:55:37'),(38,20000,20000,'2015-04-16 00:00:00','2015-04-16 18:56:03');
/*!40000 ALTER TABLE `caja` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `categoria`
--

DROP TABLE IF EXISTS `categoria`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categoria` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `Nombre_UNIQUE` (`Nombre`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categoria`
--

LOCK TABLES `categoria` WRITE;
/*!40000 ALTER TABLE `categoria` DISABLE KEYS */;
INSERT INTO `categoria` VALUES (1,'bebidas alcoholicas');
/*!40000 ALTER TABLE `categoria` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `detventa`
--

DROP TABLE IF EXISTS `detventa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `detventa` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cantidad` int(11) NOT NULL,
  `precioventa` varchar(45) NOT NULL,
  `Venta_id` int(11) NOT NULL,
  `Producto_id` int(11) NOT NULL,
  PRIMARY KEY (`id`,`Venta_id`,`Producto_id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `fk_detVenta_Venta1_idx` (`Venta_id`),
  KEY `fk_detVenta_Producto1_idx` (`Producto_id`),
  CONSTRAINT `fk_detVenta_Producto1` FOREIGN KEY (`Producto_id`) REFERENCES `producto` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_detVenta_Venta1` FOREIGN KEY (`Venta_id`) REFERENCES `venta` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `detventa`
--

LOCK TABLES `detventa` WRITE;
/*!40000 ALTER TABLE `detventa` DISABLE KEYS */;
INSERT INTO `detventa` VALUES (6,5,'4000',12,1),(7,99,'4000',12,1),(8,555,'4000',13,1),(9,15,'4000',14,1),(10,45,'4000',14,1),(11,5,'4000',15,1),(12,25,'4000',15,1),(13,5,'4000',16,1),(14,12,'4000',17,1),(16,5,'4000',19,1);
/*!40000 ALTER TABLE `detventa` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `marca`
--

DROP TABLE IF EXISTS `marca`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `marca` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(45) NOT NULL,
  `Categoria_id` int(11) NOT NULL,
  PRIMARY KEY (`id`,`Categoria_id`),
  KEY `fk_Marca_Categoria1_idx` (`Categoria_id`),
  CONSTRAINT `fk_Marca_Categoria1` FOREIGN KEY (`Categoria_id`) REFERENCES `categoria` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `marca`
--

LOCK TABLES `marca` WRITE;
/*!40000 ALTER TABLE `marca` DISABLE KEYS */;
INSERT INTO `marca` VALUES (1,'ouro fino',1);
/*!40000 ALTER TABLE `marca` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `observacion`
--

DROP TABLE IF EXISTS `observacion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `observacion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Razon` varchar(45) NOT NULL,
  `Detalles` varchar(250) NOT NULL,
  `Datetime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `Usuario_id` int(11) NOT NULL,
  PRIMARY KEY (`id`,`Usuario_id`),
  KEY `fk_Observacion_Usuario1_idx` (`Usuario_id`),
  CONSTRAINT `fk_Observacion_Usuario1` FOREIGN KEY (`Usuario_id`) REFERENCES `usuario` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `observacion`
--

LOCK TABLES `observacion` WRITE;
/*!40000 ALTER TABLE `observacion` DISABLE KEYS */;
/*!40000 ALTER TABLE `observacion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `producto`
--

DROP TABLE IF EXISTS `producto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `producto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Marca_id` int(11) NOT NULL,
  `NombreDesc` varchar(45) NOT NULL,
  `Precio` int(11) NOT NULL,
  `Cantidad` int(11) NOT NULL DEFAULT '0',
  `Codigo` varchar(45) DEFAULT NULL,
  `controlarstock` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`,`Marca_id`),
  UNIQUE KEY `CódigoProducto_UNIQUE` (`Codigo`),
  KEY `fk_Producto_Marca1_idx` (`Marca_id`),
  CONSTRAINT `fk_Producto_Marca1` FOREIGN KEY (`Marca_id`) REFERENCES `marca` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `producto`
--

LOCK TABLES `producto` WRITE;
/*!40000 ALTER TABLE `producto` DISABLE KEYS */;
INSERT INTO `producto` VALUES (1,1,'Ouro fino 354 cm3',4000,110,'78404533',1);
/*!40000 ALTER TABLE `producto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `transaccioncaja`
--

DROP TABLE IF EXISTS `transaccioncaja`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `transaccioncaja` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `monto` varchar(45) DEFAULT NULL,
  `entrada` tinyint(1) DEFAULT NULL,
  `Caja_id` int(11) NOT NULL,
  PRIMARY KEY (`id`,`Caja_id`),
  KEY `fk_EntSal_Caja1_idx` (`Caja_id`),
  CONSTRAINT `fk_EntSal_Caja1` FOREIGN KEY (`Caja_id`) REFERENCES `caja` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `transaccioncaja`
--

LOCK TABLES `transaccioncaja` WRITE;
/*!40000 ALTER TABLE `transaccioncaja` DISABLE KEYS */;
INSERT INTO `transaccioncaja` VALUES (1,'15000',1,5),(2,'15000',1,5),(3,'15000',1,5),(4,'15000',1,5),(5,'30000',1,5),(6,'20000',0,5),(7,'15000',1,5),(8,'10000',0,5),(10,'120000',1,5),(13,'12000',1,18),(14,'30000',0,18),(15,'150000',1,19);
/*!40000 ALTER TABLE `transaccioncaja` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuario`
--

DROP TABLE IF EXISTS `usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuario` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(45) NOT NULL,
  `password` varchar(45) NOT NULL,
  `privilegios` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `Nombre_UNIQUE` (`Nombre`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuario`
--

LOCK TABLES `usuario` WRITE;
/*!40000 ALTER TABLE `usuario` DISABLE KEYS */;
INSERT INTO `usuario` VALUES (1,'admin','123456',0),(2,'Vendedor01','123456',1);
/*!40000 ALTER TABLE `usuario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `venta`
--

DROP TABLE IF EXISTS `venta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `venta` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Datetime` datetime NOT NULL,
  `Usuario_id` int(11) NOT NULL,
  `Caja_id` int(11) NOT NULL,
  PRIMARY KEY (`id`,`Datetime`,`Usuario_id`,`Caja_id`),
  KEY `fk_Venta_Usuario1_idx` (`Usuario_id`),
  KEY `fk_Venta_Caja1_idx` (`Caja_id`),
  CONSTRAINT `fk_Venta_Caja1` FOREIGN KEY (`Caja_id`) REFERENCES `caja` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Venta_Usuario1` FOREIGN KEY (`Usuario_id`) REFERENCES `usuario` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `venta`
--

LOCK TABLES `venta` WRITE;
/*!40000 ALTER TABLE `venta` DISABLE KEYS */;
INSERT INTO `venta` VALUES (12,'2015-04-14 01:18:24',1,5),(13,'2015-04-14 01:18:32',1,5),(14,'2015-04-14 20:46:12',1,5),(15,'2015-04-14 23:52:13',1,19),(16,'2015-04-15 22:50:45',1,22),(17,'2015-04-15 22:51:43',1,22),(18,'2015-04-15 22:53:09',1,22),(19,'2015-04-15 22:54:56',1,22);
/*!40000 ALTER TABLE `venta` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-04-16 18:57:57
