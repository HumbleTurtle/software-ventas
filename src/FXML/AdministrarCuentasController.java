/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FXML;

import FXML.secundarios.Cuenta;
import FXML.secundarios.GvarController;
import FXML.secundarios.Helper.tClass;
import static FXML.secundarios.Static.Gen.newAlertCss;
import static FXML.secundarios.Static.GenHelper.newCol;
import clases.SenPreparada;
import java.io.IOException;
import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import static javafx.collections.FXCollections.observableArrayList;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Label;
import javafx.scene.control.TableView;
import javafx.stage.Stage;

/**
 *
 * @author Manabe
 */
public class AdministrarCuentasController extends GvarController {
    @FXML TableView<Cuenta> table_users;
    private ObservableList<Cuenta> Lista_Usuarios= observableArrayList();
    
    @FXML
    public void MostrarNuevaCuenta() throws IOException{
        Stage ventana = smanager.mostrar(smanager.NUEVO_USUARIO);
        if(ventana != null){
            ventana.showAndWait();
            actualizarCuentas();
        }
    }
    
    @FXML
    public void MostrarEditarCuenta() throws IOException{
        Cuenta acc = table_users.getSelectionModel().getSelectedItem();
        if(acc != null){
            Map<String,tClass>Usuario = new HashMap();
            Usuario.put("nombre",new tClass( acc.getNombre() ));
            Usuario.put("id",new tClass( acc.getId() ));
            Stage ventana = smanager.mostrarParam(smanager.EDITAR_USUARIO,Usuario);
            if(ventana != null){
                ventana.showAndWait();
                actualizarCuentas();
            }
        }else{
            Alert dialog = newAlertCss(AlertType.INFORMATION);
            dialog.setTitle("Información.");
            dialog.setHeaderText("Para realizar dicha acción, debe seleccionar una cuenta.");
            dialog.setContentText(null);
            dialog.showAndWait();
        }
    }
    
    @Override
    public void afterInit(){
        actualizarCuentas();
    }
    
    @FXML
    public void borrarCuenta(){
        if(table_users.getSelectionModel().getSelectedItem() != null){
            
            Cuenta acc = table_users.getSelectionModel().getSelectedItem();
            SenPreparada sen = smanager.newSenPreparada();
            sen.preparar("Select usuario.id from usuario inner join (Select usuario.id as j from venta,usuario where venta.usuario_id = usuario.id union Select usuario.id as j from observacion,usuario where observacion.usuario_id = usuario.id ) as n on usuario.id = n.j and usuario.id = ?");
            try {
                if(sen.aI(1,acc.getId()).ejecutarSentencia() && sen.getRS().next() ){
                    Alert dialog = newAlertCss(AlertType.ERROR);
                    dialog.setTitle("Error");
                    dialog.setHeaderText("El usuario no puede ser borrado porque está en uso.");
                    dialog.showAndWait();
                }else{
                    if (acc.getId() != smanager.getIdUser() ){
                        sen.preparar("Delete usuario from usuario where id= ?");
                        if ( sen.aI(1, acc.getId()).actualizar() ){
                            Alert dialog = newAlertCss(AlertType.INFORMATION);
                            dialog.setTitle("Borrado exitoso");
                            dialog.setHeaderText("El usuario se ha borrado correctamente");
                            dialog.showAndWait();
                            actualizarCuentas();
                        }
                    }else{
                        Alert dialog = newAlertCss(AlertType.ERROR);
                        dialog.setTitle("Error");
                        dialog.setHeaderText("No puedes borrar tu propia cuenta!");
                        dialog.showAndWait();
                    }
                }

            } catch (SQLException ex) {
                Logger.getLogger(AdministrarCuentasController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        table_users.getColumns().clear();
        table_users.setPlaceholder(new Label("No se ha agregado ningúna cuenta"));
        table_users.setItems(Lista_Usuarios);
        table_users.getColumns().addAll(newCol("ID","id"),newCol("Nombre de usuario","nombre"),newCol("Privilegios","sprivilegios"));
    }
    
    public void actualizarCuentas(){
        Lista_Usuarios.clear();
        
        String Query = "Select * from usuario";
        
        SenPreparada s = smanager.newSenPreparada();
        s.preparar(Query);
        if( s.ejecutarSentencia() ){
            ResultSet r = s.getRS();
            try {
                while(s.getRS().next()){
                    Lista_Usuarios.add(new Cuenta(r.getInt("id"),r.getString("nombre"),r.getInt("privilegios")));
                }
            } catch (SQLException ex) {
                Logger.getLogger(AdministrarCuentasController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
