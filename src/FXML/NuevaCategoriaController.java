package FXML;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import FXML.secundarios.GvarController;
import static FXML.secundarios.Static.Func.sinEspaciosB;
import static FXML.secundarios.Static.Func.sinSimbolos;
import static FXML.secundarios.Static.Gen.newAlertCss;
import clases.SenPreparada;
import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.TextField;

/**
 *
 * @author Manabe
 */
public class NuevaCategoriaController extends GvarController{
    @FXML TextField CAMPO_NOMBRE;
    
    @FXML void Guardar(){
        String Query = "Insert into CATEGORIA(Nombre) VALUES (?)";
        SenPreparada s = smanager.newSenPreparada();

        if ( sinEspaciosB(CAMPO_NOMBRE.getText()) && sinSimbolos(CAMPO_NOMBRE.getText()) ){
            s.preparar("Select * from CATEGORIA where habilitado = 1 and nombre=?");
            s.aS(1,CAMPO_NOMBRE.getText().toLowerCase()).ejecutarSentencia();
            try {
                
                if(!s.getRS().next() ){
                    if(s.preparar(Query)){
                        if( s.aS(1, CAMPO_NOMBRE.getText().toLowerCase() ).actualizar() ){
                            Alert msg = newAlertCss(AlertType.INFORMATION);
                            msg.setTitle("Guardado Exitoso");
                            msg.setHeaderText(null);
                            msg.setContentText("Se ha guardado la categoría con éxito.");
                            msg.showAndWait();
                            Salir();
                        }
                    }
                }else{
                    Alert msg = newAlertCss(AlertType.ERROR);
                    msg.setTitle("Error");
                    msg.setHeaderText("Es posible que esa categoría ya exista.");
                    msg.setContentText("");
                    msg.showAndWait();
                }
            } catch (SQLException ex) {
                Logger.getLogger(NuevaCategoriaController.class.getName()).log(Level.SEVERE, null, ex);
            }
            
        }else{
            Alert msg = newAlertCss(AlertType.ERROR);
             msg.setTitle("Error");
             msg.setHeaderText("Por favor retíre los dobles espacios, el espacio al principio y los símbolos.");
             msg.setContentText("");
             msg.showAndWait();
        }
    }

   
    @Override
    public void initialize(URL location, ResourceBundle resources) {

    
    }
}
