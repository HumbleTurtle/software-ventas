/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FXML;

import FXML.secundarios.GvarController;
import FXML.secundarios.Helper.tClass;
import FXML.secundarios.StageE;
import static FXML.secundarios.Static.Func.sinSimbolosEspaciosB;
import static FXML.secundarios.Static.Func.soloNumeros;
import static FXML.secundarios.Static.Gen.newAlertCss;
import static FXML.secundarios.Static.GenHelper.Capitalize;
import clases.SenPreparada;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;

/**
 *
 * @author Manabe
 */
public class EditarPromocionController extends GvarController{
    @FXML Label campoProducto;
    @FXML TextField campoNombreDesc,campoPrecio,campoCantidad;
    @FXML Pane dragPane;
    Drag drag;
    /*** Datos del producto a seleccionar / seleccionado ***/
    private int ProductoId;
    private String ProductoNombre;
    
    /*** Datos de la promocion siendo editada ***/
    private int PromocionId;

    @FXML void seleccionarProducto() throws IOException, SQLException{        
        StageE ventana= smanager.mostrarSE(smanager.SELECCIONAR_PRODUCTO);        
        if(ventana != null){
            ventana.stage.showAndWait();
            Map<String,tClass> prms = ventana.fx.<GvarController>getController().params;
            if( prms.containsKey("id-producto") ){
                SenPreparada sen = smanager.newSenPreparada();
                sen.preparar("Select id,nombredesc from producto where id=?");
                if(sen.aI(1,(int)prms.get("id-producto").get() ).ejecutarSentencia() && sen.getRS().next() ){                    
                    ProductoId = (int)prms.get("id-producto").get() ;
                    ProductoNombre = Capitalize(sen.getRS().getString("nombredesc"));
                    campoProducto.setText(ProductoNombre);                    
                }
            }            
        }        
    }
     @FXML void Guardar() throws SQLException{
        if( (!campoNombreDesc.getText().isEmpty() && campoNombreDesc.getText().length() > 3 ) && sinSimbolosEspaciosB(campoNombreDesc.getText())
            && ( !campoPrecio.getText().isEmpty() && soloNumeros(campoPrecio.getText()) && Integer.parseInt(campoPrecio.getText()) != 0 ) 
            && ( !campoCantidad.getText().isEmpty() && soloNumeros(campoCantidad.getText()) && Integer.parseInt(campoCantidad.getText()) != 0 )
            && ( ProductoId != -255 ) )
        {
            SenPreparada PromExiste=smanager.newSenPreparada();
            PromExiste.preparar("Select id,nombredesc from promocion where nombredesc=? and id != ?");
            
            if(PromExiste.aS(1, campoNombreDesc.getText().toLowerCase() ).aI(2, PromocionId).ejecutarSentencia() && PromExiste.getRS().next() ){
                Alert msg = newAlertCss(Alert.AlertType.ERROR);
                msg.setTitle("Error de guardado.");
                msg.setHeaderText("Ya existe una promoción con ese nombre.");
                msg.setContentText(null);
                msg.showAndWait();
                return;
            }
            
            SenPreparada sen = smanager.newSenPreparada();
            sen.preparar("Update promocion set nombredesc=?,cantidad=?,preciototal=?,producto_id=?");
            
            if( sen.aS(1, campoNombreDesc.getText().toLowerCase() ).aS(2, campoCantidad.getText()).aS(3, campoPrecio.getText()).aI(4, ProductoId).actualizar() ){
                Alert msg = newAlertCss(Alert.AlertType.INFORMATION);
                msg.setTitle("Promoción actualizaba.");
                msg.setHeaderText("La promoción se ha actualizado.");
                msg.setContentText(null);
                msg.showAndWait();
                Salir();
            }
        }else{
                Alert msg = newAlertCss(Alert.AlertType.INFORMATION);
                msg.setTitle("Error de llenado.");
                msg.setHeaderText("Por favor, complete correctamente el formulario.");
                msg.setContentText("Nombre Descripción - Sin símbolos, espacios iniciales, o dobles espacios intermedios y con mas de 3 caractéres.\n Precio y canitdad, sólo números y diferentes de 0. \n Seleccione un producto.");
                msg.showAndWait();   
        }
    }
    @Override
    public void afterInit(){
        if(params != null)
            if(params.containsKey("id-promocion")){
                PromocionId = (int) params.get("id-promocion").get();
                SenPreparada sen = smanager.newSenPreparada();
                sen.preparar("Select * from promocion join producto on promocion.producto_id = producto.id where promocion.id = ? and promocion.habilitado = 1");
                try {
                    if(sen.aI(1,PromocionId).ejecutarSentencia() && sen.getRS().next() ){
                        campoNombreDesc.setText(Capitalize(sen.getRS().getString("promocion.nombredesc")));
                        campoPrecio.setText(""+sen.getRS().getInt("promocion.preciototal"));
                        campoCantidad.setText(""+sen.getRS().getInt("promocion.cantidad"));
                        ProductoId = sen.getRS().getInt("promocion.producto_id");
                        ProductoNombre = sen.getRS().getString("producto.nombredesc");
                        campoProducto.setText(Capitalize(ProductoNombre));
                    }
                } catch (SQLException ex) {
                    Logger.getLogger(EditarPromocionController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
    }
    
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        drag = new Drag();
        dragPane.addEventFilter(MouseEvent.MOUSE_PRESSED, (MouseEvent evt)->{
            if(evt.getButton() == MouseButton.PRIMARY ){
                drag.iX=evt.getX();
                drag.iY=evt.getY();
            }
        });

        dragPane.addEventFilter(MouseEvent.MOUSE_DRAGGED, (MouseEvent evt)->{
            if(evt.getButton()== MouseButton.PRIMARY){
                getStage().setX(evt.getScreenX()-drag.iX);
                getStage().setY(evt.getScreenY()-drag.iY);
            }
        });
    }
}
