/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FXML;

import FXML.secundarios.GvarController;
import FXML.secundarios.MarcaCat;
import FXML.secundarios.Static;
import static FXML.secundarios.Static.Gen.newAlertCss;
import clases.SenPreparada;
import java.io.IOException;
import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import static javafx.collections.FXCollections.observableArrayList;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ListCell;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import javafx.util.StringConverter;

/**
 *
 * @author Manabe
 */
public class EditarMarcaController extends GvarController{
    
    @FXML TextField CampoNombre;
    @FXML ComboBox<MarcaCat>CampoCategoria;
    ObservableList<MarcaCat> Lista_Categorias;
    private MarcaCat Marca,Categoria;
    
    private int IdSelectedCat;
    
    @Override
    public void afterInit(){
        CargarCategorias();    
        
        if( params.get("id-marca") != null ){
            Marca = new MarcaCat();
            Categoria = new MarcaCat();
            
            Marca.Id = (int) params.get("id-marca").get();
            
            SenPreparada sen = smanager.newSenPreparada();
            sen.preparar("SELECT * FROM marca join categoria on marca.categoria_id = categoria.id and categoria.habilitado = 1 where marca.id = ?");
            try {
                if(sen.aI(1,Marca.Id).ejecutarSentencia() && sen.getRS().next()){
                    ResultSet rs = sen.getRS();
                    Marca.setNombre(rs.getString("marca.nombre") );
                    Categoria.Id= rs.getInt("categoria.id");
                    Categoria.setNombre( rs.getString("categoria.nombre") );
                    
                    /**Seteando los valores a la ventana**/
                    CampoNombre.setText(Marca.Nombre);
                    Lista_Categorias.stream().filter((s) -> (s.Id == Categoria.Id )).forEach((s) -> {
                        CampoCategoria.getSelectionModel().select(s);
                        IdSelectedCat = s.Id;
                    });
                }
            } catch (SQLException ex) {
                Logger.getLogger(EditarMarcaController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    @FXML
    void Guardar(){
        if( CampoNombre.getLength() > 0 && CampoNombre.getText().matches("(?! )([A-z0-9áéíóúÁÉÍÓÚÄËÏÖÜäëïöü](?![ ]{2}) *)+") 
           && CampoCategoria.getSelectionModel().getSelectedItem() != null
        ){
           SenPreparada sen = smanager.newSenPreparada();
           sen.preparar("Select * from marca where habilitado = 1 and categoria_id = ? and nombre = ? Limit 1");
           
            try {
                if( sen.aI(1, CampoCategoria.getSelectionModel().getSelectedItem().Id ).aS(2, CampoNombre.getText().toLowerCase() ).ejecutarSentencia() && !sen.getRS().next() ){
                    sen = smanager.newSenPreparada();
                    sen.preparar("Update marca set Nombre = ?, categoria_id = ? where id = ? Limit 1");
                    if( sen.aS(1, CampoNombre.getText().toLowerCase() ).aI(2,CampoCategoria.getSelectionModel().getSelectedItem().Id).aI(3, Marca.Id).actualizar() ){
                        Alert msg = newAlertCss(Alert.AlertType.INFORMATION);
                        msg.setTitle("Guardado Exitoso");
                        msg.setHeaderText(null);
                        msg.setContentText("Se ha guardado la marca con éxito.");
                        msg.showAndWait();
                        Salir();
                    }
                    
                }else{
                    Alert msg = newAlertCss(Alert.AlertType.ERROR);
                    msg.setTitle("Error");
                    msg.setHeaderText("Es posible que esa marca ya exista.");
                    msg.setContentText("");
                    msg.showAndWait();
                }
            } catch (SQLException ex) {
                Logger.getLogger(EditarMarcaController.class.getName()).log(Level.SEVERE, null, ex);
            }

        }else{
            Alert msg = newAlertCss(Alert.AlertType.ERROR);
            msg.setTitle("Error de llenado");
            msg.setHeaderText("Por favor, complete todos los campos correctamente.");
            msg.setContentText("No se permiten los dobles espacios, espacios iniciales, finales ni los símbolos en el nombre. Debe llenar cada uno de los campos.");
            msg.showAndWait();
        
        }

        
    }
    
    @FXML void MostrarNuevaCategoria() throws IOException{
        Stage ventana = smanager.mostrar(smanager.NUEVA_CATEGORIA);        
        if(ventana != null){
            ventana.showAndWait();
            CargarCategorias();
        }
    }
    
    private void CargarCategorias(){
        String nombrecat,Query = "Select * from Categoria where habilitado = 1";
        int idcat;
        SenPreparada s = smanager.newSenPreparada();
        
        s.preparar(Query);
        s.ejecutarSentencia();
        
        if(CampoCategoria.getSelectionModel().getSelectedItem() != null)
            IdSelectedCat = CampoCategoria.getSelectionModel().getSelectedItem().Id;
        else 
            IdSelectedCat = -1;
        
        Lista_Categorias.clear();
        try {
            while(s.getRS().next()){
                idcat = s.getRS().getInt("categoria.id");
                nombrecat = s.getRS().getString("categoria.nombre");
                Lista_Categorias.add(new MarcaCat(idcat,nombrecat));
            }
            reSelect(IdSelectedCat);
        } catch (SQLException ex) {
            Logger.getLogger(NuevaMarcaController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    void reSelect(int sel){

        if(sel != -1 ){
            Lista_Categorias.stream().filter((s) -> (s.Id == sel )).forEach((s) -> {
                CampoCategoria.getSelectionModel().select(s);
            });
        }
    }
    
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        Lista_Categorias = observableArrayList();    
        CampoCategoria.setItems(Lista_Categorias);
        CampoCategoria.getSelectionModel().select(1);
        
        CampoCategoria.setCellFactory((combo)->{
             return new ListCell<MarcaCat>() {
                     @Override
                     protected void updateItem(MarcaCat item, boolean empty) {
                             super.updateItem(item, empty);

                             if (item == null || empty) {
                                     setText(null);
                             } else {
                                     setText(item.Nombre);
                             }
                     }
             };
        });

        CampoCategoria.setConverter(new StringConverter<MarcaCat>() {
             @Override
             public String toString(MarcaCat item) {
                     if (item == null) {
                             return null;                             
                     } else {
                             return item.Nombre;
                     }
             }

             @Override
             public MarcaCat fromString(String personString) {
                     return null; // No conversion fromString needed.
             }
         });


    }
    
    
}
