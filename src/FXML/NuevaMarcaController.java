/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FXML;

import FXML.secundarios.MarcaCat;
import FXML.secundarios.GvarController;
import static FXML.secundarios.Static.Gen.newAlertCss;
import clases.SenPreparada;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import static javafx.collections.FXCollections.observableArrayList;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ListCell;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import javafx.util.StringConverter;

/**
 *
 * @author Manabe
 */
public class NuevaMarcaController extends GvarController{
    @FXML TextField CAMPO_NOMBRE;
    @FXML ComboBox<MarcaCat> comboCategoria;
    ObservableList<MarcaCat> Lista_Categorias;
    private int IdSelectedCat;
    
    @FXML void Guardar(){
        String Query = "Insert into MARCA(Nombre,categoria_id) VALUES (?,?)";
        MarcaCat categoria = null;
        
        SenPreparada s = smanager.newSenPreparada();
        
        if(Lista_Categorias.size()>=1){
            categoria = comboCategoria.getSelectionModel().getSelectedItem();
        
            if (comboCategoria.getSelectionModel().getSelectedItem() != null && 
                CAMPO_NOMBRE.getText().matches("(?! )([A-z0-9áéíóúÁÉÍÓÚÄËÏÖÜäëïöü](?![ ]{2}) *)+") ){
                s.preparar("Select * from MARCA where habilitado = 1 and categoria_id= ? and nombre=?");
                s.aS(2,CAMPO_NOMBRE.getText().toLowerCase()).aI(1, categoria.Id).ejecutarSentencia();
                try {

                    if(!s.getRS().next() ){
                        if(s.preparar(Query)){
                            if( s.aS(1, CAMPO_NOMBRE.getText().toLowerCase() ).aI(2, categoria.Id).actualizar() ){
                                Alert msg = newAlertCss(AlertType.INFORMATION);
                                msg.setTitle("Guardado Exitoso");
                                msg.setHeaderText(null);
                                msg.setContentText("Se ha guardado la marca con éxito.");
                                msg.showAndWait();
                                Salir();
                            }
                        }
                    }else{
                        Alert msg = newAlertCss(AlertType.ERROR);
                        msg.setTitle("Error");
                        msg.setHeaderText("Es posible que esa marca ya exista.");
                        msg.setContentText("");
                        msg.showAndWait();
                    }
                } catch (SQLException ex) {
                    Logger.getLogger(NuevaMarcaController.class.getName()).log(Level.SEVERE, null, ex);
                }

            }else{
                Alert msg = newAlertCss(AlertType.ERROR);
                 msg.setTitle("Error");
                 msg.setHeaderText("Por favor retíre los dobles espacios, el espacio al principio y los símbolos.");
                 msg.setContentText("También asegúrese de llenar todos los campos.");
                 msg.showAndWait();
            }
        }
    }
    private void CargarCategorias(){
        String nombrecat,Query = "Select * from Categoria where habilitado = 1";
        int idcat;
        SenPreparada s = smanager.newSenPreparada();
        
        s.preparar(Query);
        s.ejecutarSentencia();
        
        if(comboCategoria.getSelectionModel().getSelectedItem() != null)
            IdSelectedCat = comboCategoria.getSelectionModel().getSelectedItem().Id;
        else 
            IdSelectedCat = -1;
        
        Lista_Categorias.clear();
        try {
            while(s.getRS().next()){
                idcat = s.getRS().getInt("categoria.id");
                nombrecat = s.getRS().getString("categoria.nombre");
                Lista_Categorias.add(new MarcaCat(idcat,nombrecat));
            }
            reSelect(IdSelectedCat);
        } catch (SQLException ex) {
            Logger.getLogger(NuevaMarcaController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    @FXML void MostrarNuevaCategoria() throws IOException{
        Stage ventana = smanager.mostrar(smanager.NUEVA_CATEGORIA);        
        if(ventana != null){
            ventana.showAndWait();
            CargarCategorias();
        }
    }
    
    @Override
    public void afterInit(){
        CargarCategorias();  
    }
    
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        
        Lista_Categorias = observableArrayList();      
        comboCategoria.setItems(Lista_Categorias);
        comboCategoria.getSelectionModel().select(1);
        
        comboCategoria.setCellFactory((combo)->{
             return new ListCell<MarcaCat>() {
                     @Override
                     protected void updateItem(MarcaCat item, boolean empty) {
                             super.updateItem(item, empty);

                             if (item == null || empty) {
                                     setText(null);
                             } else {
                                     setText(item.Nombre);
                             }
                     }
             };
        });

        comboCategoria.setConverter(new StringConverter<MarcaCat>() {
             @Override
             public String toString(MarcaCat item) {
                     if (item == null) {
                             return null;                             
                     } else {
                             return item.Nombre;
                     }
             }

             @Override
             public MarcaCat fromString(String personString) {
                     return null; // No conversion fromString needed.
             }
         });

    }

    void reSelect(int sel){

        if(sel != -1 ){
            Lista_Categorias.stream().filter((s) -> (s.Id == sel )).forEach((s) -> {
                comboCategoria.getSelectionModel().select(s);
            });
        }
    }
}
