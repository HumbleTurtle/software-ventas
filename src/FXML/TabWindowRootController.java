/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FXML;

import FXML.secundarios.GvarController;
import static FXML.secundarios.Static.Gen.newAlertCss;
import clases.SenPreparada;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;

import static java.nio.file.StandardCopyOption.*;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.Timer;
import java.util.TimerTask;
import static javafx.application.ConditionalFeature.FXML;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Rectangle2D;
import javafx.scene.Parent;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.stage.FileChooser;
import javafx.stage.Screen;
import javafx.stage.Stage;

/**
 *
 * @author Manabe
 */
public class TabWindowRootController extends GvarController{
    @FXML Parent VentanaInventario,VentanaVenta;
    @FXML InventarioController VentanaInventarioController;
    @FXML FacturacionController VentanaVentaController;
    @FXML AnchorPane anchorInv;
    @FXML HBox hBoxMenu;
    
    @FXML ScrollPane scrollPane1,scrollPane2;
    boolean abriendo = false;
    boolean cerrando = false;
    @FXML void abrirMenu(){
            if(abriendo)
                return;
            Timer animTimer = new Timer();
            animTimer.scheduleAtFixedRate(new TimerTask() {

            int i=0;

            @Override
            public void run() {
                if (hBoxMenu.getPrefHeight() < 145  && ( abriendo || (!abriendo && i == 0 ) ) ){
                    hBoxMenu.setPrefHeight(hBoxMenu.getPrefHeight()+3);
                    abriendo = true;
                    cerrando = false;
                }
                else {
                    this.cancel();
                    abriendo = false;
                }

                i++;
            }
        }, 100, 15);

    }
    
    @FXML void cerrarMenu(){
            Timer animTimer = new Timer();
            animTimer.scheduleAtFixedRate(new TimerTask() {

            int i=0;

            @Override
            public void run() {
                if (hBoxMenu.getPrefHeight() >= 3 && ( cerrando || (!cerrando && i == 0 ) )  ){
                    hBoxMenu.setPrefHeight(hBoxMenu.getPrefHeight()-3);
                    abriendo= false;
                    cerrando= true;
                }
                else {
                    this.cancel();
                    cerrando = false;
                }

                i++;
            }
        }, 200, 10);
    }
    
    @FXML void mostrarRegistroVentas() throws IOException{
       Stage ventana = smanager.mostrar(smanager.REGISTRO_VENTAS);
       if(ventana != null){
            ventana.show();
       }
    }
    @FXML
    public void mostrarVerPromociones() throws IOException{
       Stage ventana = smanager.mostrar(smanager.VER_PROMOCIONES);
       if(ventana != null){
            ventana.show();
       }
    }
    
    @FXML
    public void mostrarCaja() throws IOException{
       Stage ventana = smanager.mostrar(smanager.CAJA);
       if(ventana != null){
            ventana.show();
       }
    }
    
    @FXML
    public void mostrarRegistroCajas() throws IOException{
       Stage ventana = smanager.mostrar(smanager.REGISTRO_CAJAS);
       if(ventana != null){
            ventana.show();
       }
    }
    
    @FXML
    public void mostrarInventario() throws IOException{
       Stage ventana = smanager.mostrar(smanager.INVENTARIO);
       if(ventana != null){
            ventana.show();
       }
    }
    
    @FXML void realizarBackup() throws IOException{
        
        Alert confirmar = newAlertCss( Alert.AlertType.CONFIRMATION );
        confirmar.setTitle("Confirmar acción.");
        confirmar.setHeaderText("EL programa está a punto de realizar un backup de la base de datos.");
        confirmar.setContentText("Este proceso cerrará la caja y reiniciará el programa.");
        
        Optional<ButtonType> op = confirmar.showAndWait();
        
        if (op.get() == ButtonType.OK){
            final FileChooser fileChooser = new FileChooser();

            fileChooser.setTitle("Buscar backup");
            fileChooser.setInitialDirectory(
                new File(System.getProperty("user.home"))
             );                 
            fileChooser.getExtensionFilters().addAll(
            new FileChooser.ExtensionFilter("SQL", "*.sql"),
            new FileChooser.ExtensionFilter("GZ", "*.gz") );
            File file = fileChooser.showSaveDialog(getStage());
            if (file != null) {
                SenPreparada sen = smanager.newSenPreparada();
                sen.preparar("Update caja set cierra = ?,fechacierra = now() where id = ?");
                if( sen.aI(1, smanager.getCierraCaja()).aI(2,smanager.getIdCaja()).actualizar() ){
                    Alert msg = newAlertCss(Alert.AlertType.INFORMATION);
                    msg.setTitle("Caja cerrada.");
                    msg.setHeaderText("La caja se ha cerrado correctamente. El programa se reiniciará.");
                    msg.setContentText(null);
                    msg.showAndWait();
                    smanager.reiniciar();
                }	
                Runtime.getRuntime().exec(new java.io.File("").getAbsolutePath()+"\\mysqldump.exe -u root ferfran > "+file.getAbsolutePath());

                String[] command = new String[] {"cmd.exe", "/c", "\""+new java.io.File("").getAbsolutePath()+"\\mysqldump.exe\" -u root ferfran > "+file.getAbsolutePath()};
                Process process = Runtime.getRuntime().exec(command);
                Alert msg = newAlertCss(Alert.AlertType.INFORMATION);
                msg.setTitle("Caja cerrada.");
                msg.setHeaderText(new java.io.File("").getAbsolutePath()+"\\mysqldump.exe -u root -p 126bh57yqfW ferfran > "+file.getAbsolutePath()+file.getName());
                msg.setContentText(null);
                msg.showAndWait();
                
            }

        }
    }
    
    
    @FXML void cambiarUsuario(){
        Alert confirmar = newAlertCss( Alert.AlertType.CONFIRMATION );
        confirmar.setTitle("Confirmar acción.");
        confirmar.setHeaderText("¿Está seguro de querer cambiar de usuario?.");
        confirmar.setContentText(null);
        
        Optional<ButtonType> op = confirmar.showAndWait();
        
        if (op.get() == ButtonType.OK){
            smanager.reiniciando(true);
            getStage().close();
            smanager.reiniciar();
        }
    }
    
    @FXML
    public void mostrarAdministrarUsuario() throws IOException{
        Stage ventana = smanager.mostrar(smanager.ADMN_USUARIO);
        if(ventana != null){
            ventana.show();
        }
    }
    @FXML
    void mostrarAdministrarMarcas() throws IOException{
        Stage ventana = smanager.mostrar(smanager.ADMN_MARCA);
        if(ventana != null){
            ventana.show();
        }
    }
    
    @FXML
    void mostrarAdministrarCategorias() throws IOException{
        Stage ventana = smanager.mostrar(smanager.ADMN_CATEGORIA);
        if(ventana != null){
            ventana.show();
        }
    }
    
    @Override
    public void afterInit(){
        Rectangle2D primaryScreenBounds = Screen.getPrimary().getVisualBounds();
        getStage().setX(primaryScreenBounds.getMinX());
        getStage().setY(primaryScreenBounds.getMinY());
        getStage().setWidth(primaryScreenBounds.getWidth());
        getStage().setHeight(primaryScreenBounds.getHeight());
        
        //VentanaInventario.autosize();
        VentanaInventario.setPickOnBounds(true);
        VentanaVenta.setPickOnBounds(true);

        getStage().heightProperty().addListener( (evt,oldheight,newheight)->{
            if(newheight.doubleValue() > 1000){
                scrollPane1.setFitToHeight(true);
                scrollPane2.setFitToHeight(true);
            }else{
                scrollPane1.setFitToHeight(false);
                scrollPane2.setFitToHeight(false); 
            }
        });
 

        smanager.initializeEmbedFXML(VentanaInventarioController, getStage() );
        smanager.initializeEmbedFXML(VentanaVentaController, getStage() );
    }
    
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        
    }
    
}
