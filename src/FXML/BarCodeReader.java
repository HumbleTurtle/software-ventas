/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FXML;


import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.CopyOnWriteArrayList;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;





/**
 *
 * @author Manabe
 */

public class BarCodeReader{
    Timer t;
    TimerTask Task;
    long FREQ,RELOJ,PARAMETRO;
    int z;
    String BARCODE;
    
    private final List<BarCodeListener> listeners = new CopyOnWriteArrayList<BarCodeListener>();
    
    
    public interface BarCodeListener{
        public void fire(String s);
    }
    
    BarCodeReader(){
        t = new Timer();
        FREQ = 10; RELOJ = 0;
        PARAMETRO = 80;
        
        z=1;
        BARCODE = "";
        
        Task = new TimerTask(){
            @Override
            public void run() {
                RELOJ+=10;
            }            
        };
        t.scheduleAtFixedRate(Task, 0, FREQ);
        
    }
    
    private void timerLoop(){
        t.schedule(Task, FREQ);
    }
    
    public void printCurrent(){
       //System.out.println(RELOJ);
    }

    public void typed(KeyEvent c){

        if(c.getCode()== KeyCode.ENTER){
            if( BARCODE.length() >= 8 ){
                fireBarCode(BARCODE);
                z=1;
                BARCODE = "";
            }
        }else{
            if(RELOJ <= PARAMETRO || z == 1){
                BARCODE+=c.getText();
                z++;
            }else{     
                z = 1;
                
            }

        }
        RELOJ = 0;  
    }
    public void addListener(BarCodeListener listener){
        listeners.add(listener);
    }
    public void removeListener(BarCodeListener listener){
        listeners.remove(listener);
    }
    public void fireBarCode(String s){
        for(BarCodeListener listener : listeners)
            listener.fire(s);
    }
    
}
