/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FXML;

import FXML.secundarios.Cuenta;
import FXML.secundarios.GvarController;
import FXML.secundarios.Static;
import FXML.secundarios.Static.CUENTAS;
import static FXML.secundarios.Static.Func.*;
import static FXML.secundarios.Static.Gen.newAlertCss;
import clases.SenPreparada;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ListCell;
import javafx.scene.control.TextField;
import javafx.util.StringConverter;

/**
 *
 * @author Manabe
 */
public class NuevoUsuarioController extends GvarController {
    @FXML ComboBox<Cuenta> campo_privilegios;
    @FXML TextField campo_nombre,campo_password;
    @FXML public void Guardar(){
        String nombre = campo_nombre.getText();
        String password = campo_password.getText();
        
        if( (nombre.length() > 3 &&  sinSimbolosEspaciosB(nombre) && noVacio(nombre) )
            &&(noVacio(password)&& sinEspaciosA(password) && password.length() > 5 ) 
            && campo_privilegios.getSelectionModel().getSelectedItem() != null )
            {
                SenPreparada sen = smanager.newSenPreparada();
                
                String Query = "Insert into usuario(nombre,password,privilegios) VALUES (?,?,?)";
                int privilegios  =  campo_privilegios.getSelectionModel().getSelectedItem().getPrivilegios().intValue();
                
                sen.preparar(Query);
                
                if(sen.aS(1, nombre).aS(2, password).aI(3, privilegios).actualizar()){
                    Alert s = newAlertCss(AlertType.INFORMATION);
                    s.setTitle("Guardado exitoso.");
                    s.setHeaderText("Se ha creado la cuenta "+nombre+" correctamente");
                    s.showAndWait();  
                    getStage().close();
                }
            }
        else{
            Alert s = newAlertCss(AlertType.WARNING);
            s.setTitle("Error de llenado.");
            s.setHeaderText("Por favor, complete todos los campos correctamente");
            s.setContentText("No se premiten espacios en el nombre y en el password.\n"
                    + "Debe también asegurarse de que todos los campos estén completos."+
                    "La contraseña debe tener más de 5 caractéres.");
            s.showAndWait();
        }
            
    
    
    }
    
    @Override
    public void afterInit(){
        cargaCuentas();
    }
    
    
    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }
    
    private void cargaCuentas(){
        campo_privilegios.setItems(CUENTAS.get());
        
        campo_privilegios.setCellFactory((obj)->{
             return new ListCell<Cuenta>() {
                     @Override
                     protected void updateItem(Cuenta item, boolean empty) {
                             super.updateItem(item, empty);

                             if (item == null || empty) {
                                     setText(null);
                             } else {
                                     setText(item.getNombre());
                             }
                     }
             };
        });

        campo_privilegios.setConverter(new StringConverter<Cuenta>() {
             @Override
             public String toString(Cuenta item) {
                     if (item == null) {
                             return null;
                     } else {
                             return item.getNombre();
                     }
             }

             @Override
             public Cuenta fromString(String personString) {
                     return null; // No conversion fromString needed.
             }
         });   
    }
    
}
