/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FXML;

import FXML.secundarios.GvarController;
import FXML.secundarios.Helper.tClass;
import FXML.secundarios.MarcaCat;
import FXML.secundarios.Static;
import static FXML.secundarios.Static.Gen.newAlertCss;
import static FXML.secundarios.Static.GenHelper.Capitalize;
import static FXML.secundarios.Static.GenHelper.newCol;
import clases.SenPreparada;
import java.io.IOException;
import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import static javafx.collections.FXCollections.observableArrayList;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.TableView;
import javafx.stage.Stage;

/**
 *
 * @author Manabe
 */
public class AdministrarCategoriasController extends GvarController{
    @FXML TableView<MarcaCat> tablaCategorias;
    ObservableList Lista_Categorias;
    
    @FXML void mostrarNuevaCategoria() throws IOException{
        Stage ventana = smanager.mostrar(smanager.NUEVA_CATEGORIA);
        if(ventana != null){
            ventana.showAndWait();
            actualizarTabla();
        }
    }
    
    @FXML void mostrarEditarCategoria() throws IOException{
        if(tablaCategorias.getSelectionModel().getSelectedItem() != null){
            MarcaCat tb = tablaCategorias.getSelectionModel().getSelectedItem();
            Map<String,tClass> prms = new HashMap<>();      
            prms.put("id-cat", new tClass((int)tb.Id) );
            
            Stage ventana = smanager.mostrarParam(smanager.EDITAR_CATEGORIA, prms);
            if(ventana != null){
                ventana.showAndWait();
                actualizarTabla();
            }
        }

    
    }
    
    @FXML void borrarCategoria(){
        if( Lista_Categorias.indexOf( tablaCategorias.getSelectionModel().getSelectedItem() ) != -1 ){
           MarcaCat Obj = tablaCategorias.getSelectionModel().getSelectedItem();
           SenPreparada sen = smanager.newSenPreparada();
           sen.preparar("Select * from marca where marca.habilitado = 1 and marca.categoria_id = ?");
            try {
                if(sen.aI(1, Obj.Id).ejecutarSentencia() && sen.getRS().next()){
                    Alert dialog = newAlertCss(AlertType.ERROR);
                    dialog.setTitle("Error");
                    dialog.setHeaderText("La categoria no puede ser borrada porque está en uso.");
                    dialog.setContentText("Elimine o modifique todos las marcas que la utilicen para continuar.");
                    dialog.showAndWait();
                }else{
                    sen.preparar("UPDATE categoria set habilitado = 0 where id= ?");
                    if ( sen.aI(1, Obj.Id).actualizar() ){
                        Alert dialog = newAlertCss(AlertType.INFORMATION);
                        dialog.setTitle("Borrado exitoso");
                        dialog.setHeaderText("La categoria se ha borrado correctamente");
                        dialog.showAndWait();
                        actualizarTabla();
                    }
                }
            } catch (SQLException ex) {
                Logger.getLogger(AdministrarMarcasController.class.getName()).log(Level.SEVERE, null, ex);
            }

        }    
    }
    private void actualizarTabla(){
    
        SenPreparada sen = smanager.newSenPreparada();
        sen.preparar("Select * from categoria where categoria.habilitado = 1");
        Lista_Categorias.clear();
        if(sen.ejecutarSentencia() ){
            try {
                ResultSet r = sen.getRS();
                while(sen.getRS().next()){                    
                    Lista_Categorias.add(new MarcaCat(r.getInt("id"),r.getString("nombre")));
                }
            } catch (SQLException ex) {
                Logger.getLogger(AdministrarMarcasController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        Lista_Categorias = observableArrayList();

        tablaCategorias.setItems(Lista_Categorias);
        tablaCategorias.getColumns().addAll(newCol("Id", "Id"),newCol("Categoria", "Nombre"));

    }
    @Override
    public void afterInit(){
        actualizarTabla();
    }
}
