/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FXML;

import FXML.secundarios.MarcaCat;
import FXML.secundarios.GvarController;
import static FXML.secundarios.Static.Gen.newAlertCss;
import clases.SenPreparada;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import static javafx.collections.FXCollections.observableArrayList;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ListCell;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import javafx.util.StringConverter;




/**
 *
 * @author Manabe
 */
public class NuevoProductoController extends GvarController{
    @FXML TextField nombreDesc,campoCod;
    @FXML ComboBox<Boole> controlarStock;
    @FXML ComboBox<MarcaCat> marcaid,categoriaid;
    @FXML TextField precio;
    @FXML Pane dragPane;
    
    Drag drag;
    ObservableList<MarcaCat> Lista_Marcas,Lista_Categorias;
    
    ObservableList<Boole> Lista_Controlar;
    
    @FXML void NuevaMarca() throws IOException{
        Stage ventana = smanager.mostrar(smanager.NUEVA_MARCA);        
        if(ventana != null){
            ventana.showAndWait();
            
            SqlRequestCategoria();
            SqlRequestMarca();
        }  
    }

    
    @FXML public void Guardar(){
        String Query; boolean Preparado = false;
        
        SenPreparada sen = smanager.newSenPreparada();
        /*Validaciones*/
        if(marcaid.getSelectionModel().getSelectedIndex() != -1 
           && precio.getText().replace(" ", "").matches("[0-9]*")
           && nombreDesc.getText().length()> 3
           && nombreDesc.getText().matches("(?! )([A-z0-9áéíóúÁÉÍÓÚÄËÏÖÜäëïöü](?![ ]{2}) *)+") 
           && ( campoCod.getLength() > 3 || campoCod.getLength() == 0 )
           &&  controlarStock.getSelectionModel().getSelectedItem() != null 
        )
        {  
            String codigo = null;
            if(campoCod.getText().length() > 0){
                codigo = campoCod.getText();                
            }
            

            
            
            if( !campoCod.getText().matches("^(FFA)([1-9])+") ){
                    
                    SenPreparada checkearExistencia = smanager.newSenPreparada();
                    checkearExistencia.preparar("Select id from producto where codigo = ?");
                    try {
                        if(checkearExistencia.aS(1, campoCod.getText()).ejecutarSentencia() && checkearExistencia.getRS().next())
                        {
                            Alert alert = newAlertCss(AlertType.WARNING);
                            alert.setTitle("Complete el formulario correctamente");
                            alert.setHeaderText("Complete todos los campos.");
                            alert.setContentText("Ese código ya está siendo utilizado");
                            
                            alert.showAndWait();
                            return;
                        }
                    } catch (SQLException ex) {
                        Logger.getLogger(NuevoProductoController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    Query = "Insert into producto (Marca_Id,NombreDesc,Precio,codigo,controlarstock) VALUES (?,?,?,?,?)";

                    sen.preparar(Query);
                    MarcaCat marca = marcaid.getSelectionModel().selectedItemProperty().get();
                    MarcaCat cat = categoriaid.getSelectionModel().selectedItemProperty().get();

                    if( sen
                        .aI(1, marca.Id)
                        .aI(3, precio.getText().replace(" ", "") )
                        .aS(2, nombreDesc.getText())
                        .aS(4, codigo)
                        .aB(5, controlarStock.getSelectionModel().getSelectedItem().getValue())
                        .actualizar() )

                    {
                        Alert alert = newAlertCss(AlertType.INFORMATION);
                        alert.setTitle("Alerta");
                        alert.setHeaderText(null);
                        alert.setContentText("Guardado Exitoso. " );
                        alert.showAndWait();
                        Salir();

                    }else{
                        Alert alert = newAlertCss(AlertType.ERROR);
                        alert.setTitle("Alerta");
                        alert.setHeaderText(null);
                        alert.setContentText("Error de guardado.");
                        alert.showAndWait();
                        Salir();
                    }
                    
                
                
            }else{
                Alert alert = newAlertCss(AlertType.WARNING);
                alert.setTitle("Complete el formulario correctamente");
                alert.setHeaderText("Complete todos los campos.");
                alert.setContentText("No puede utilizar la nomenclatura FFA[Número] como código. pues está reservada por el programa y se asigna automáticamente a los productos sin código");
                alert.showAndWait(); 
            }
        }else{
                        Alert alert = newAlertCss(AlertType.WARNING);
                        alert.setTitle("Complete el formulario correctamente");
                        alert.setHeaderText("Complete todos los campos.");
                        alert.setContentText("Elimine los dobles espacios, los espacios iniciales y los símbolos de los campos de texto.\n"
                                + "Sólo se permiten números en los campos de precio y/o cantidad"
                                + "Los campos de nombre/descripción y código deben ser nulos o con más de 3 caractéres/cifras");
                        alert.showAndWait(); 
        }
    }
    @Override
    public void afterInit(){
               
       CargaMarcaCat();
       CargaControlar();
       
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {

       categoriaid.setOnAction((evt)->{
           SqlRequestMarca();
       });
    }
    private void SqlRequestMarca(){
        Lista_Marcas.clear();
        if(categoriaid.getSelectionModel().getSelectedIndex() !=-1){
            MarcaCat categoria = categoriaid.getSelectionModel().getSelectedItem();
            
            
            String Query_Marca ="Select * from Marca where habilitado = 1 and categoria_id = ?";
            SenPreparada sen =  smanager.newSenPreparada();
            if(sen.preparar(Query_Marca)){
                if(sen.aI(1, categoria.Id).ejecutarSentencia()){
                    try {

                        while(sen.getRS().next() ){
                            MarcaCat m = new MarcaCat(sen.getRS().getInt("id"),sen.getRS().getString("nombre").substring(0,1).toUpperCase()+ sen.getRS().getString("nombre").substring(1));
                            Lista_Marcas.add(m);

                        }                    

                    } catch (SQLException ex) {
                        Logger.getLogger(NuevoProductoController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }

       }
    }
    
    private void SqlRequestCategoria(){
        Lista_Categorias.clear();
        String Query_Categoria ="Select * from Categoria where habilitado = 1";
        SenPreparada sen =  smanager.newSenPreparada();
        if(sen.preparar(Query_Categoria)){
            if(sen.ejecutarSentencia()){

                try {
                    while(sen.getRS().next() ){
                        MarcaCat m = new MarcaCat(sen.getRS().getInt("id"),sen.getRS().getString("nombre").substring(0,1).toUpperCase()+ sen.getRS().getString("nombre").substring(1));
                        Lista_Categorias.add(m);
                    }
                    
                    
                } catch (SQLException ex) {
                    Logger.getLogger(NuevoProductoController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

    }
    private void CargaMarcaCat(){
    /***Carga marca***/
        
        Lista_Marcas =  observableArrayList();
        Lista_Categorias = observableArrayList();
        
        SqlRequestMarca();
        SqlRequestCategoria();   
        
        marcaid.setCellFactory((combo)->{
             return new ListCell<MarcaCat>() {
                     @Override
                     protected void updateItem(MarcaCat item, boolean empty) {
                             super.updateItem(item, empty);

                             if (item == null || empty) {
                                     setText(null);
                             } else {
                                     setText(item.Nombre);
                             }
                     }
             };
        });

        marcaid.setConverter(new StringConverter<MarcaCat>() {
             @Override
             public String toString(MarcaCat item) {
                     if (item == null) {
                             return null;
                     } else {
                             return item.Nombre;
                     }
             }

             @Override
             public MarcaCat fromString(String personString) {
                     return null; // No conversion fromString needed.
             }
         });
        
        categoriaid.setCellFactory((combo)->{
             return new ListCell<MarcaCat>() {
                     @Override
                     protected void updateItem(MarcaCat item, boolean empty) {
                             super.updateItem(item, empty);

                             if (item == null || empty) {
                                     setText(null);
                             } else {
                                     setText(item.Nombre);
                             }
                     }
             };
        });

        categoriaid.setConverter(new StringConverter<MarcaCat>() {
             @Override
             public String toString(MarcaCat item) {
                     if (item == null) {
                             return null;
                     } else {
                             return item.Nombre;
                     }
             }

             @Override
             public MarcaCat fromString(String personString) {
                     return null; // No conversion fromString needed.
             }
         });       
        
        categoriaid.setItems(Lista_Categorias);
        marcaid.setItems(Lista_Marcas);
    }
 
    private void CargaControlar(){
        Lista_Controlar = observableArrayList();
        
        Lista_Controlar.add(new Boole(true) );
        Lista_Controlar.add(new Boole(false) ); 

        controlarStock.setItems(Lista_Controlar);

        controlarStock.setCellFactory((combo)->{
             return new ListCell<Boole>() {
            @Override
            protected void updateItem(Boole item, boolean empty) {
                    super.updateItem(item, empty);
                    if(item == null || empty)
                        setText(null);
                    else
                        if(!item.getValue())
                            setText("No");
                        else
                            setText("Sí");
                    
                        
                }
            };
        });
        controlarStock.setConverter(new StringConverter<Boole>() {
            @Override
            public String toString(Boole item) {
                    if (item == null) {
                            return null;
                    } else {
                        if(item.getValue())
                            return "Sí";
                        else
                            return "No";
                    }
         }

         @Override
         public Boole fromString(String string) {
                 return null; // No conversion fromString needed.
         }
     });
        
    drag = new Drag();
    dragPane.addEventFilter(MouseEvent.MOUSE_PRESSED, (MouseEvent evt)->{
        if(evt.getButton() == MouseButton.PRIMARY ){
            drag.iX=evt.getX();
            drag.iY=evt.getY();
        }
    });

    dragPane.addEventFilter(MouseEvent.MOUSE_DRAGGED, (MouseEvent evt)->{
        if(evt.getButton()== MouseButton.PRIMARY){
            getStage().setX(evt.getScreenX()-drag.iX);
            getStage().setY(evt.getScreenY()-drag.iY);
        }
    });
        
        
    }
    
    
    class Boole{
        private boolean value;
        Boole(boolean s){
            value= s;
        }
        public boolean getValue(){
            return value;
        }
    }
    


}
