/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FXML;

import FXML.secundarios.GvarController;
import static FXML.secundarios.Static.Gen.newAlertCss;
import clases.SenPreparada;
import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;

/**
 *
 * @author Manabe
 */
public class ActualizarStockController extends GvarController {
    @FXML Label campoProducto,campoCantidadActual;
    @FXML TextField campoCantidad;
    @FXML ToggleGroup agregarQuitar;
    @FXML Pane dragPane;
    
    Drag drag;

    @Override
    public void afterInit(){
        if( params.get("id-producto") != null ){
            SenPreparada sen = smanager.newSenPreparada();
            sen.preparar("Select NombreDesc,Cantidad from producto where id = ? LIMIT 1");
            try {
                if ( sen.aI(1, (int)params.get("id-producto").get() ).ejecutarSentencia() && sen.getRS().next() ){
                    campoProducto.setText( sen.getRS().getString("NombreDesc") );
                    campoCantidadActual.setText( sen.getRS().getString("Cantidad") );
                }
            } catch (SQLException ex) {
                Logger.getLogger(ActualizarStockController.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
    }
    
    
    @FXML void Guardar() throws SQLException{
        if( !campoCantidad.getText().isEmpty() && campoCantidad.getText().matches("([0-9])+") ){
            if( params.get("id-producto") != null ){
                int idproducto = (int) params.get("id-producto").get();
                String sAgregarQuitar = ( (RadioButton)agregarQuitar.getSelectedToggle() ).getText();

                if( sAgregarQuitar.equals("Agregar") ){
                    SenPreparada sen = smanager.newSenPreparada();
                    String Query = "Update producto join (Select id,(cantidad + ? )as proc from producto where id = ?)as x on x.id = producto.id set producto.cantidad = x.proc ";
                    sen.preparar(Query);

                    if(sen.aI(1, campoCantidad.getText() ).aI(2, idproducto).actualizar() ){
                        Alert dialog = newAlertCss(AlertType.INFORMATION);
                        dialog.setTitle("Cambios guardados con éxito.");
                        dialog.setHeaderText("Existencias actualizadas.");
                        dialog.setContentText(null);
                        dialog.showAndWait();
                        Salir();
                    }
                } else if(sAgregarQuitar.equals("Quitar")){
                    SenPreparada sen1 = smanager.newSenPreparada();
                    sen1.preparar("Select id from producto where cantidad >= ? and id = ? Limit 1");
                    if(sen1.aI(1, campoCantidad.getText() ).aI(2, idproducto).ejecutarSentencia() && sen1.getRS().next() ){
                        SenPreparada sen = smanager.newSenPreparada();
                        String Query = "Update producto join (Select id,(cantidad - ? )as proc from producto where id = ?)as x on x.id = producto.id set producto.cantidad = x.proc ";
                        sen.preparar(Query);

                        if(sen.aI(1, campoCantidad.getText() ).aI(2, idproducto).actualizar() ){
                            Alert dialog = newAlertCss(AlertType.INFORMATION);
                            dialog.setTitle("Cambios guardados con éxito.");
                            dialog.setHeaderText("Existencias actualizadas.");
                            dialog.setContentText(null);
                            dialog.showAndWait();
                            Salir();
                        }

                    }else{
                            Alert dialog = newAlertCss(AlertType.ERROR);
                            dialog.setTitle("Error de validación");
                            dialog.setHeaderText("La cantidad que desea quitar, excede la cantidad actual.");
                            dialog.setContentText(null);
                            dialog.showAndWait();

                }
            }
        }else{
            Alert dialog = newAlertCss(AlertType.ERROR);
            dialog.setTitle("Error de validación.");
            dialog.setHeaderText("Solo se permiten números el campo cantidad.");
            dialog.setContentText(null);
            dialog.showAndWait();
        }
      }
    }
    

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        drag = new Drag();
        dragPane.addEventFilter(MouseEvent.MOUSE_PRESSED, (MouseEvent evt)->{
            if(evt.getButton() == MouseButton.PRIMARY ){
                drag.iX=evt.getX();
                drag.iY=evt.getY();
            }
        });
        
        dragPane.addEventFilter(MouseEvent.MOUSE_DRAGGED, (MouseEvent evt)->{
            if(evt.getButton()== MouseButton.PRIMARY){
                getStage().setX(evt.getScreenX()-drag.iX);
                getStage().setY(evt.getScreenY()-drag.iY);
            }
        });
    }
    
    
}
