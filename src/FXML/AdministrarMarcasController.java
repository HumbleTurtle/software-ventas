/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FXML;

import FXML.secundarios.GvarController;
import FXML.secundarios.Helper.tClass;
import FXML.secundarios.MarcaCat;
import FXML.secundarios.Static;
import static FXML.secundarios.Static.Gen.newAlertCss;
import static FXML.secundarios.Static.GenHelper.Capitalize;
import static FXML.secundarios.Static.GenHelper.newCol;
import clases.SenPreparada;
import java.io.IOException;
import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import static javafx.collections.FXCollections.observableArrayList;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.TableView;
import javafx.stage.Stage;

/**
 *
 * @author Manabe
 */
public class AdministrarMarcasController extends GvarController{
    @FXML TableView<TableObj> tablaMarcas;
    ObservableList Lista_Marcas;
    
    @FXML void mostrarNuevaMarca() throws IOException{
        Stage ventana = smanager.mostrar(smanager.NUEVA_MARCA);
        if(ventana != null){
            ventana.showAndWait();
            actualizarTabla();
        }
    }
    
    @FXML void mostrarEditarMarca() throws IOException{
        if(tablaMarcas.getSelectionModel().getSelectedItem() != null){
            TableObj tb = tablaMarcas.getSelectionModel().getSelectedItem();
            Map<String,tClass> prms = new HashMap<>();      
            prms.put("id-marca", new tClass((int)tb.IdMarca) );
            
            Stage ventana = smanager.mostrarParam(smanager.EDITAR_MARCA, prms);
            if(ventana != null){
                ventana.showAndWait();
                actualizarTabla();
            }
        }

    
    }
    
    @FXML void borrarMarca(){
        if(Lista_Marcas.indexOf(tablaMarcas.getSelectionModel().getSelectedItem() ) != -1 ){
           TableObj Obj = tablaMarcas.getSelectionModel().getSelectedItem();
           SenPreparada sen = smanager.newSenPreparada();
           sen.preparar("Select * from producto where producto.habilitado = 1 and producto.marca_id = ?");
            try {
                if(sen.aI(1, Obj.IdMarca).ejecutarSentencia() && sen.getRS().next()){
                    Alert dialog = newAlertCss(AlertType.ERROR);
                    dialog.setTitle("Error");
                    dialog.setHeaderText("La marca no puede ser borrada porque está en uso.");
                    dialog.setContentText("Elimine o modifique todos los productos que la utilicen para continuar.");
                    dialog.showAndWait();
                }else{
                    sen.preparar("UPDATE marca set habilitado = 0 where id = ?");
                    if ( sen.aI(1, Obj.IdMarca).actualizar() ){
                        Alert dialog = newAlertCss(AlertType.INFORMATION);
                        dialog.setTitle("Borrado exitoso");
                        dialog.setHeaderText("La marca se ha borrado correctamente");
                        dialog.showAndWait();
                        actualizarTabla();
                    }
                }
            } catch (SQLException ex) {
                Logger.getLogger(AdministrarMarcasController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }    
        
    }
    private void actualizarTabla(){
    
        SenPreparada sen = smanager.newSenPreparada();
        sen.preparar("Select categoria.nombre, marca.nombre,marca.id,marca.habilitado from marca inner join categoria on categoria.id = marca.categoria_id WHERE marca.habilitado = 1 ORDER BY marca.id DESC");
        Lista_Marcas.clear();
        if(sen.ejecutarSentencia() ){
            try {
                ResultSet r = sen.getRS();
                while(sen.getRS().next()){                    
                    Lista_Marcas.add(new TableObj(r.getInt("marca.id"),r.getString("marca.nombre"),r.getString("categoria.nombre")));
                }
            } catch (SQLException ex) {
                Logger.getLogger(AdministrarMarcasController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    @Override
    public void afterInit(){
        actualizarTabla();
    }
    
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        Lista_Marcas = observableArrayList();
        tablaMarcas.setItems(Lista_Marcas);
        tablaMarcas.getColumns().addAll(newCol("Id", "IdMarca"),newCol("Marca", "NombreMarca"),newCol("Categoria", "NombreCat"));
    }
    public class TableObj{
        private final String NombreCat,NombreMarca;
        final int IdMarca;
        TableObj(int Id,String NombreMarca,String NombreCat){
            this.IdMarca = Id;
            this.NombreMarca = NombreMarca;
            this.NombreCat = NombreCat;
        }
        public String getNombreCat(){
            return Capitalize(NombreCat);
        }
        public String getNombreMarca(){
            return Capitalize(NombreMarca);
        }
        public int getIdMarca(){
            return IdMarca;
        }
    }
}
