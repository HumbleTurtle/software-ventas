/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FXML;

import FXML.secundarios.GvarController;
import FXML.secundarios.Helper.tClass;
import FXML.secundarios.Promocion;
import static FXML.secundarios.Static.Gen.newAlertCss;
import static FXML.secundarios.Static.GenHelper.newCol;
import clases.SenPreparada;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import static javafx.collections.FXCollections.observableArrayList;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.TableView;
import javafx.stage.Stage;

/**
 *
 * @author Manabe
 */
public class VerPromocionesController extends GvarController {
    @FXML TableView<Promocion> tablaPromociones;    
    ObservableList<Promocion> Lista_Promociones;
    
    @FXML void borrarPromocion(){
        if(tablaPromociones.getSelectionModel().getSelectedItem() != null ){
            Alert confirmar = newAlertCss( Alert.AlertType.CONFIRMATION );
            confirmar.setTitle("Confirmar acción.");
            confirmar.setHeaderText("¿Está seguro de borrar ésta promoción?.");
            confirmar.setContentText(null);

            Optional<ButtonType> op = confirmar.showAndWait();

            if (op.get() == ButtonType.OK){
                Promocion p = tablaPromociones.getSelectionModel().getSelectedItem();
                SenPreparada sen = smanager.newSenPreparada();
                sen.preparar("Update promocion set habilitado = 0 where id = ?");
                if(sen.aI(1, p.getId() ).actualizar()){
                    Alert msg = newAlertCss( Alert.AlertType.INFORMATION );
                    msg.setTitle("Borrado exitoso.");
                    msg.setHeaderText("Se ha borrado la promoción con éxito.");
                    msg.setContentText(null);
                    msg.showAndWait();
                    actualizar();
                }
            }
        }
    }
    
    @FXML void mostrarNuevaPromocion() throws IOException{
        Stage ventana = smanager.mostrar(smanager.NUEVA_PROMOCION);
        if(ventana != null){
            ventana.showAndWait();
            actualizar();
        }
    }
    
    @FXML void mostrarEditarPromocion() throws IOException{
        
        if(tablaPromociones.getSelectionModel().getSelectedItem() != null ){
            int Id = tablaPromociones.getSelectionModel().getSelectedItem().getId();
            Map<String,tClass> prms = new HashMap<>();
            prms.put("id-promocion", new tClass(Id) );
            Stage ventana = smanager.mostrarParam(smanager.EDITAR_PROMOCION,prms);
            
            if(ventana != null){
                ventana.showAndWait();
                actualizar();
            }
        }

    }    
    
    private void actualizar(){
        SenPreparada sen = smanager.newSenPreparada();
        sen.preparar("Select * from promocion join producto on producto.id = promocion.producto_id where promocion.habilitado = 1");
        if(sen.ejecutarSentencia())
            try {
                Lista_Promociones.clear();
                while(sen.getRS().next()){
                    Lista_Promociones.add(new Promocion(sen.getRS().getInt("Promocion.id"),sen.getRS().getString("Promocion.nombredesc"),sen.getRS().getString("producto.nombredesc"),sen.getRS().getInt("Promocion.cantidad"),sen.getRS().getInt("promocion.preciototal")));
                }
            }   catch (SQLException ex) {
            Logger.getLogger(VerPromocionesController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    @Override
    public void afterInit(){
        actualizar();
    }
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        Lista_Promociones = observableArrayList();
        tablaPromociones.setPlaceholder(new Label("No se han creado promociones."));
        tablaPromociones.getColumns().clear();
        tablaPromociones.setItems(Lista_Promociones);
        tablaPromociones.getColumns().addAll(newCol("Código","Cod"),newCol("Nombre / Descripción","StrNombre"),newCol("Producto","StrNombreProducto"),newCol("Precio","StrPrecioTotal"),newCol("Cantidad a vender","Cantidad"));
    }
        
}
