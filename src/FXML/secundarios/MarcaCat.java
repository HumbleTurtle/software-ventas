/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FXML.secundarios;

import static FXML.secundarios.Static.GenHelper.Capitalize;

/**
 *
 * @author Manabe
 */
public class MarcaCat{
    public int Id;
    public String Nombre;

    public MarcaCat(int id, String nombre) {
        Id = id;
        Nombre = Capitalize(nombre); 
    }

    public MarcaCat() {
    }

    
    public void setNombre(String nombre){
        Nombre = Capitalize(nombre);
    }
    
    public String getNombre(){
        return Nombre;
    }
    public int getId(){
        return Id;
    }
}