/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FXML.secundarios;

import static FXML.secundarios.Static.GenHelper.Capitalize;
import static FXML.secundarios.Static.GenHelper.KFormat;

/**
 *
 * @author Manabe
 */
public class TransaccionCaja {
    
    private boolean entrada;
    private int monto;
    private String usuario;
    
    public TransaccionCaja(String usuario,boolean ent,int mont){
        entrada = ent;
        monto = mont;
        this.usuario = Capitalize(usuario);
        
    }
    
    public String getMonto(){
        return KFormat(monto)+"Gs.";
    }
    public int getIntMonto(){
        return monto;
    }
    
    public String getUsuario(){
        return usuario;
    }
    
    public String getEntrada(){
        if(entrada)
            return "Ingreso de dinero a la caja";
        else
            return "Salida de dinero de la caja";
    }
    
}
