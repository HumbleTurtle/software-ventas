/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FXML.secundarios;

import FXML.ferfran;
import java.io.File;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import static javafx.collections.FXCollections.observableArrayList;
import javafx.collections.ObservableList;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TextInputDialog;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.FileChooser;

/**
 *
 * @author Manabe
 */
public class Static {
    public static class CONSTANTES{
        public static final int CIERRADEFAULT = -255;
        public static final int ABREDEFAULT = 0;
    }
    
    
    
    
    public static class CUENTAS{
        public static final int ADMINISTRADOR = 0,VENDEDOR = 1,CUALQUIERA = 5;
        private static final Map<Integer,String> StringCuenta = new HashMap();
        private static final ObservableList<Cuenta> Tipos = observableArrayList();       
        
        static{
            StringCuenta.put(ADMINISTRADOR,"ADMINISTRADOR");
            StringCuenta.put(VENDEDOR,"VENDEDOR");
            Tipos.add(new Cuenta("ADMINISTRADOR",ADMINISTRADOR));
            Tipos.add(new Cuenta("VENDEDOR",VENDEDOR));
        }
        /** Retorna la lista de tipos de cuenta ya cargadas **/
        public static ObservableList get(){
            return Tipos;
        }
        
        public static String getName(int id){
            return StringCuenta.get(id);
        }
    }
    /**Funciones estáticas de validación**/
    public static class Func{
        public static boolean soloNumeros(String s){
            if(s.matches("([0-9])+") )
                return true;
            return false;
        }
        
        /**Retorna true si no se encuentra vacía, en caso contrario retornará false**/
        public static boolean noVacio(String s){
            if(s.length() != 0 )
                return true;
            return false;
        }
        /**Retorna true cuando no encuentra ningún símbolo, en caso contrario retornará false.**/
        public static boolean sinSimbolos(String s){
            if(s.matches("([A-z0-9áéíóúÁÉÍÓÚÄËÏÖÜäëïöü ]+)"))
                return true;
            return false;
        }
        
        /**Se permiten espacios intermedios, pero no al principio ni al final.**/
        public static boolean sinEspaciosB(String s){
            if(s.matches("((?![ ])^([\\S ]*[\\S]+)(?! )$)"))
                return true;
            return false;
        }
        /**Retorna true cuando no encuentra ningún espacio o símbolo, en caso contrario retornará false.**/
        public static boolean sinSimbolosEspaciosA(String s){
            if(sinSimbolos(s) && sinEspaciosA(s))
                return true;
            return false;
        }
        /**Retorna true cuando no encuentra ningún espacio inicial o final y dobles espacios intermedios, permite un espacio entre cada palabra. False cuando encuentra alguno de los mencionados.**/
        public static boolean sinSimbolosEspaciosB(String s){
            if(s.matches("((?![ ])^([\\wáéíóúÁÉÍÓÚÄËÏÖÜäëïöü. ]*[\\wáéíóúÁÉÍÓÚÄËÏÖÜäëïöü.]+)(?! )$)"))
                return true;
            return false;
        }
        /**Retorna true cuando no encuentra ningún espacio y false cuando lo encuentra.**/
        public static boolean sinEspaciosA(String s){
            if(s.matches("(\\S*(?! )*$)*"))
                return true;
            return false;
        }
    }
    
    public static class GenHelper{
        public static TableColumn newCol(String nombre,String nombreVar){
            TableColumn s = new TableColumn(nombre);        
            s.setCellValueFactory(new PropertyValueFactory<>(nombreVar));
            return s;
        }
        public static String Capitalize(String s){
           if(s.length() >= 2)
            return s.substring(0, 1).toUpperCase()+s.substring(1);
           else
               return s.toUpperCase();
        }
        
        public static String KFormat(int i){
            DecimalFormat format = new DecimalFormat("###,###.###");
            return format.format(i);
        }
    }

    
    public static class Gen{

        
        public static Alert newAlertCss(AlertType prm){
            Alert alert = new Alert(prm);
            alert.getDialogPane().getStylesheets().add(ferfran.class.getResource("fxml/css/hoja.css").toExternalForm());
            return alert;
        }
        
        public static TextInputDialog newTextInputCss(){
            TextInputDialog dialog = new TextInputDialog("");
            dialog.getDialogPane().getStylesheets().add(ferfran.class.getResource("fxml/css/hoja.css").toExternalForm());
            return dialog;
        }
        
        public static TextInputDialog newTextInputCss(String s){
            TextInputDialog dialog = new TextInputDialog(s);
            dialog.getDialogPane().getStylesheets().add(ferfran.class.getResource("fxml/css/hoja.css").toExternalForm());
            return dialog;
        }
    }
}
