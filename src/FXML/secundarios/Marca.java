/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FXML.secundarios;

/**
 *
 * @author Manabe
 */
class Marca{
    public String Id;
    public String Nombre;
    public String CategoriaId;
    public Marca(String id, String nombre,String categoriaid) {
        Id = id;
        Nombre = nombre.substring(0, 1).toUpperCase()+nombre.substring(1);
        CategoriaId=categoriaid;
    }
}