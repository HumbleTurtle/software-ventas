/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FXML.secundarios;

import FXML.secundarios.Static.CUENTAS;
import javafx.beans.property.SimpleStringProperty;

/**
 *
 * @author Manabe
 */
public class Cuenta {
    private SimpleStringProperty nombre;
    private Number privilegios;
    private String sprivilegios;
    private int id;
    public Cuenta(String nombre,int privilegios){
        this.nombre = new SimpleStringProperty(nombre);
        this.privilegios = privilegios;
        this.sprivilegios = CUENTAS.getName(privilegios);
    }
    public Cuenta(int id,String nombre,int privilegios){
        this.id=id;
        this.nombre = new SimpleStringProperty(nombre);
        this.privilegios = privilegios;
        this.sprivilegios = CUENTAS.getName(privilegios);
    }

    
    public int getId(){
        return id;
    }    
    
    public String getNombre(){
        return nombre.get();
    }

    public Number getPrivilegios(){
        return privilegios;
    }
    
    public String getSprivilegios(){
        return sprivilegios;
    }
    
    public void setId(int i){
        id= i;
    }
    public void setNombre(String s){
        nombre.set(s);
    }
    
    public void setPrivilegios(int i){
        privilegios = i;
    }
    public void setSprivilegios(String s){
        sprivilegios = s;
    }
}
