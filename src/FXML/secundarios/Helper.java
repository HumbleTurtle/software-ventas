/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FXML.secundarios;

/**
 *
 * @author Manabe
 */
public class Helper {
    public static class tClass<T>{
        private T value;
        public tClass(){}
        public tClass(T v){ value = v; }

        public void set(T v){
            value = v;
        }
        public T get(){
            return value;
        }        
    }
}
