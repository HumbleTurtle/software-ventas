/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FXML.secundarios;

import FXML.secundarios.Helper.tClass;
import clases.SenPreparada;
import java.net.URL;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import static javafx.collections.FXCollections.observableArrayList;
import javafx.collections.ObservableList;
import javafx.fxml.Initializable;
import javafx.scene.Scene;

import javafx.stage.Stage;

/**
 *
 * @author Manabe
 */
public abstract class GvarController implements Initializable {
    
    public StageManager smanager;
    
    public Map<String,tClass> params;
    
    private Stage thisStage;
    
    private ObservableList<SenPreparada> ListaSens;
    

    public void setStage(Stage s){
        thisStage =s;
    }
    

    
    public Stage getStage(){
        return thisStage;
    }
    

    public void Salir(){
        thisStage.close();

    }
    
    @Override
    public abstract void initialize(URL location, ResourceBundle resources);
    public void afterInit(){};
}
