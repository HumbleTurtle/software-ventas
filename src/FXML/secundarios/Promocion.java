/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FXML.secundarios;

import static FXML.secundarios.Static.GenHelper.Capitalize;
import static FXML.secundarios.Static.GenHelper.KFormat;

/**
 *
 * @author Manabe
 */
public class Promocion {
    private final String nombre,nombreproducto;
    private final int id;
    private final int cantidad;
    private final int preciototal;
    
    public Promocion(int id, String nombre,String nombreproducto, int cantidad, int preciototal){
        this.id=id;
        this.nombre = nombre;
        this.cantidad = cantidad;
        this.preciototal = preciototal;
        this.nombreproducto = nombreproducto;
    }
    
    public int getId(){
        return id;
    }    
    
    public String getCod(){
        return "PROM"+id;
    }
    
    public int getCantidad(){
        return cantidad;
    }
    
    public int getPrecioTotal(){
        return preciototal;
    }
    
    public String getStrPrecioTotal(){
        return KFormat(preciototal) + "Gs.";
    }
    public String getStrNombre(){
        return Capitalize(nombre);
    }
    public String getStrNombreProducto(){
        return Capitalize(nombreproducto);
    }
}
