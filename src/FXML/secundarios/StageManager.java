/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FXML.secundarios;

import FXML.LoginController;
import FXML.secundarios.Helper.tClass;
import static FXML.secundarios.Static.CUENTAS.VENDEDOR;
import static FXML.secundarios.Static.CUENTAS.ADMINISTRADOR;
import static FXML.secundarios.Static.CUENTAS.CUALQUIERA;
import static FXML.secundarios.Static.Gen.newAlertCss;
import clases.Conexion;
import clases.SenPreparada;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Application;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import static javafx.scene.input.KeyCode.T;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/**
 *
 * @author Manabe
 */
public class StageManager{
    private final List<Stage> stagelist;

    private int User_Id;
    private String User_Name;
    private String User_Password;
    private int User_Privileges;
    private Caja caja;
    private boolean Reiniciando;
    private Conexion conexion;

    
    private boolean Logeado;
    
    private Application app;
    
    public final Ventana LOGIN,NUEVA_VENTA,REGISTRO_VENTAS,INVENTARIO,NUEVO_PRODUCTO,EDITAR_PRODUCTO,NUEVA_MARCA,EDITAR_MARCA,
            NUEVA_CATEGORIA,ADMN_USUARIO,NUEVO_USUARIO,EDITAR_USUARIO,ADMN_MARCA,ADMN_CATEGORIA,EDITAR_CATEGORIA,ACTUALIZAR_STOCK,CAJA,
            REGISTRO_CAJAS,VER_PRODUCTOS_CAJA,ROOTWINDOW,VER_PRODUCTOS_VENTA,SELECCIONAR_PRODUCTO,NUEVA_PROMOCION,SELECCIONAR_PROMOCION,EDITAR_PROMOCION
            ,VER_PROMOCIONES;

    public void reiniciando(Boolean b){
        Reiniciando = b;
    }
    
    public boolean reiniciando(){
        return Reiniciando;
    }
    
    public void reiniciar(){
        conexion.cierraConexion();
        Reiniciando = true;
        conexion = new Conexion();
        User_Name = "Visitante";
        User_Password = "";
        User_Privileges = CUALQUIERA;
        Logeado = false;
        caja = new Caja(conexion);
        
        Stage ventana;
        try {
            closeall();
            stagelist.clear();
            ventana = mostrar(this.LOGIN);
            if ( ventana != null )
                ventana.show();
                
        } catch (IOException ex) {
            Logger.getLogger(StageManager.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(StageManager.class.getName()).log(Level.SEVERE, null, ex);
        }
        Reiniciando = false;
    }
    
    public boolean checkLogin(String name,String password){
        
        String Query = "Select * from Usuario where nombre = ? and password = ? LIMIT 1";
        SenPreparada sen = new SenPreparada(conexion);
        sen.preparar(Query);
        try {
            if(sen.aS(1,name).aS(2, password).ejecutarSentencia() && sen.getRS().next() ){
                    ResultSet res = sen.getRS();

                    Alert dialog = newAlertCss(Alert.AlertType.INFORMATION);
                    dialog.setTitle("Inicio de sesión correcto.");
                    dialog.setHeaderText("Bienvenido/a "+res.getString("nombre"));
                    dialog.setContentText(null);
                    dialog.showAndWait();
                    
                    User_Id = res.getInt("id");
                    User_Name = res.getString("Nombre");
                    User_Password = res.getString("Password");
                    User_Privileges = res.getInt("Privilegios");
                    Logeado = true;

                    return true;
            }
        } catch (SQLException ex) {
            Logger.getLogger(LoginController.class.getName()).log(Level.SEVERE, null, ex);
        }
     
        return false;        
    }
    

    
    public StageManager(){
        
        /***Se crea la conexion***/
        conexion = new Conexion();
        
        /******** Se inicializa **********/
        User_Name = "Visitante";
        User_Password = "";
        User_Privileges = CUALQUIERA;
        Logeado = false;
        caja = new Caja(conexion);
        

        
        /**Inicializando**/
        stagelist = new ArrayList<>();        

        
        LOGIN= new Ventana("fxml/iniciarsesion.fxml",CUALQUIERA,StageStyle.UNDECORATED);
        CAJA = new Ventana("fxml/caja.fxml",ADMINISTRADOR);
        REGISTRO_CAJAS = new Ventana("fxml/registroaperturacajas.fxml",ADMINISTRADOR);
        VER_PRODUCTOS_CAJA = new Ventana("fxml/verproductoscaja.fxml",ADMINISTRADOR);
        VER_PRODUCTOS_VENTA = new Ventana("fxml/verproductosventa.fxml",VENDEDOR);
        
        NUEVA_PROMOCION = new Ventana("fxml/nuevapromocion.fxml",ADMINISTRADOR,StageStyle.UNDECORATED);
        EDITAR_PROMOCION = new Ventana("fxml/editarpromocion.fxml",ADMINISTRADOR,StageStyle.UNDECORATED);
        VER_PROMOCIONES =  new Ventana("fxml/verpromociones.fxml",VENDEDOR);
        SELECCIONAR_PROMOCION =  new Ventana("fxml/seleccionarpromocion.fxml",VENDEDOR);
        
        ROOTWINDOW = new Ventana("fxml/tabwindowroot.fxml",VENDEDOR);
        
        NUEVA_VENTA= new Ventana("fxml/nuevaventa.fxml", VENDEDOR);
        REGISTRO_VENTAS= new Ventana("fxml/registroventas.fxml", VENDEDOR);
        SELECCIONAR_PRODUCTO = new Ventana("fxml/inventario_1.fxml",VENDEDOR);
        
        INVENTARIO= new Ventana("fxml/inventario.fxml",VENDEDOR);
        ACTUALIZAR_STOCK = new Ventana("fxml/actualizarstock.fxml", ADMINISTRADOR,StageStyle.UNDECORATED);
        NUEVO_PRODUCTO= new Ventana("fxml/nuevoproducto.fxml", ADMINISTRADOR,StageStyle.UNDECORATED);
        EDITAR_PRODUCTO = new Ventana("fxml/editarproducto.fxml",ADMINISTRADOR,StageStyle.UNDECORATED);       
        
        ADMN_MARCA = new Ventana("fxml/administrarmarcas.fxml", ADMINISTRADOR);
        NUEVA_MARCA= new Ventana("fxml/nuevamarca.fxml", ADMINISTRADOR);
        EDITAR_MARCA = new Ventana("fxml/editarmarca.fxml", ADMINISTRADOR);
        
        ADMN_CATEGORIA = new Ventana("fxml/administrarcategorias.fxml", ADMINISTRADOR);
        NUEVA_CATEGORIA= new Ventana("fxml/nuevacategoria.fxml", ADMINISTRADOR);
        EDITAR_CATEGORIA = new Ventana("fxml/editarcategoria.fxml", ADMINISTRADOR);
        
        ADMN_USUARIO= new Ventana("fxml/administradorcuentas.fxml", ADMINISTRADOR);
        NUEVO_USUARIO= new Ventana("fxml/nuevacuenta.fxml", ADMINISTRADOR);        
        EDITAR_USUARIO= new Ventana("fxml/editarcuenta.fxml",ADMINISTRADOR);

        
    }

    

    public StageE newStageFXML(String archivo) throws IOException{
        /**Carga archivo FXML en un nuevo STAGE sin definir las dimensiones**/

        
        StageE s = new StageE();
        s.fx = new FXMLLoader(FXML.ferfran.class.getResource(archivo));
        s.parent= (Parent)s.fx.load();
        s.scene=new Scene(s.parent);
        s.stage.setScene(s.scene);
        stagelist.add(s.stage);
        return s;
    }
    
  
    
    public Stage mostrar(Ventana v) throws IOException{
        if( (Logeado && User_Privileges <= v.getPriv()) || v.getPriv() == CUALQUIERA ){
            StageE s=newStageFXML(v.getRuta());
            GvarController controller = s.fx.<GvarController>getController();
            controller.smanager = this;
            controller.setStage(s.stage);

            controller.afterInit();
            s.stage.initStyle( v.vStageStyle );
            
            s.stage.setTitle("FerFranAdmin v1.0.0.0");
            s.stage.setOnCloseRequest((evt)->{
                try{ stagelist.remove(s.stage); }
                catch(Exception ex){}
                if(stagelist.isEmpty() && !Reiniciando)
                    System.exit(0);
            });

            return s.stage;
        }
        Alert dialog = newAlertCss(AlertType.WARNING);
        dialog.setTitle("Acceso denegado");
        dialog.setHeaderText("No posee los privilegios para realizar esa acción");
        dialog.setContentText(null);
        dialog.showAndWait();
        
        return null;
    }
    public StageE mostrarSE(Ventana v) throws IOException{
        if( (Logeado && User_Privileges <= v.getPriv()) || v.getPriv() == CUALQUIERA ){
            StageE s=newStageFXML(v.getRuta());
            GvarController controller = s.fx.<GvarController>getController();
            controller.smanager = this;
            controller.setStage(s.stage);

            controller.afterInit();
            s.stage.initStyle( v.vStageStyle );
            
            s.stage.setTitle("FerFranAdmin v1.0.0.0");
            
            s.stage.setOnCloseRequest((evt) ->{
                try{ stagelist.remove(s.stage); }
                catch(Exception ex){}
                if(stagelist.isEmpty() && !Reiniciando)
                    System.exit(0);

            });

            return s;
        }
        Alert dialog = newAlertCss(AlertType.WARNING);
        dialog.setTitle("Acceso denegado");
        dialog.setHeaderText("No posee los privilegios para realizar esa acción");
        dialog.setContentText(null);
        dialog.showAndWait();
        
        return null;
    }
    
    public SenPreparada newSenPreparada(){
        SenPreparada sen = new SenPreparada(conexion);
        return sen;
    }
    
    public Stage mostrarParam(Ventana v, Map<String,tClass> params) throws IOException{
        if( (Logeado && User_Privileges <= v.getPriv()) || v.getPriv() == CUALQUIERA ){
            StageE s=newStageFXML(v.getRuta());
            GvarController controller = s.fx.<GvarController>getController();
            controller.smanager = this;
            controller.setStage(s.stage);


            controller.params = params;
            controller.afterInit();
            s.stage.initStyle( v.vStageStyle );
            
            s.stage.setTitle("FerFranAdmin v1.0.0.0");
            
            s.stage.setOnCloseRequest((evt) ->{
                   try{ stagelist.remove(s.stage); }
                catch(Exception ex){}
                if(stagelist.isEmpty() && !Reiniciando)
                    System.exit(0);
            });


            return s.stage;
        }
        Alert dialog = newAlertCss(AlertType.WARNING);
        dialog.setTitle("Acceso denegado");
        dialog.setHeaderText("No posee los privilegios para realizar esa acción");
        dialog.setContentText(null);
        dialog.showAndWait();
        
        return null;
    }
    
    public void initializeEmbedFXML(GvarController controller,Stage s){
        controller.smanager = this;
        controller.setStage(s);
        Stage stage = controller.getStage();        
        
        controller.afterInit();
        
        s.setTitle("FerFranAdmin v1.0.0.0");
        stagelist.add(s);
        
        s.setOnCloseRequest((evt) ->{
            try{ stagelist.remove(s); }
            catch(Exception ex){}
            if(stagelist.isEmpty() && !Reiniciando)
                System.exit(0);
        });
    }
    
    public void closeall() throws Exception{
        
         stagelist.stream().filter(s -> s != null).forEach( ( s ) ->{
             s.close();
         });
    }   
    public void setApp (Application app){
        this.app = app;
    }
    
    public Application getApp(Application app){
        return app;
    }

    public void setUsuario(int id, String nombre,int privilegios) {
        User_Id = id;
        User_Name = nombre;
        User_Privileges = privilegios;        
    }
    
    public int getIdUser(){
        return User_Id;
    }
    
    public int getIdCaja(){
        return caja.getId();
    }
    
    public int getAbreCaja(){
        return caja.getAbre();
    }
    
    public int getCierraCaja(){
        caja.calcularDineroEnCaja();
        return caja.getCierra();
    }
    
    public String getFechaAbreCaja(){
        return caja.getFechaAbre();
    }
    
    public boolean abrirCaja(){
        if(Logeado){
            if( caja.abrirCaja() )
                return true;
        }
        return false;
    }

    private class Ventana{
        
        private final String Ruta;  /**Ruta para acceder**/
        private final int Privilegios_n;    /**Privilegios necesarios**/
        private final StageStyle vStageStyle;
        
        
        public Ventana(String r,int p){
            Ruta = r;
            Privilegios_n = p;
            vStageStyle = StageStyle.DECORATED;
        }
        public Ventana(String r, int p,StageStyle stagestyle){
            Ruta = r;
            Privilegios_n = p;
            vStageStyle = stagestyle;
        }
       
        public String getRuta(){
           return Ruta;
        }
        
        public int getPriv(){
           return Privilegios_n;
        }
    }
    
}



