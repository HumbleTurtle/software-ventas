/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FXML.secundarios;

import static FXML.secundarios.Static.GenHelper.KFormat;
import java.sql.Date;

/**
 *
 * @author Manabe
 */
public class Venta {
    private final int Id;
    private final int ValorVenta;
    private final Date Fecha;
    
    public Venta(int id, int valorventa,Date fecha){
        this.Id = id;
        this.ValorVenta = valorventa;
        this.Fecha = fecha;
    }
    
    public int getId(){
        return Id;
    }
    
    public String getValorVenta(){
        return KFormat(ValorVenta)+"Gs.";
    }
    
    public String getFecha(){
        return Fecha.toString();
    }

}
