/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FXML.secundarios;

import FXML.CajaController;
import FXML.secundarios.Static.CONSTANTES;
import static FXML.secundarios.Static.Gen.newAlertCss;
import static FXML.secundarios.Static.Gen.newTextInputCss;
import static FXML.secundarios.Static.GenHelper.KFormat;
import clases.Conexion;
import clases.SenPreparada;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.TextInputDialog;

/**
 *
 * @author Manabe
 */
public final class Caja {
    private int id;
    private int abre; /**Dinero en caja al abrir**/
    private Timestamp fechaAbre;
    private Timestamp fechaCierra;
    
    private Conexion conexion;
    
    private int cierra;/**Dinero en caja al cerrar**/
    private final boolean READONLY;
    
    public Caja(Conexion conn ){READONLY = false; conexion = conn; }
    
    public Caja(int id,int abre,int cierra, Timestamp fAbre,Timestamp fCierra){
        this.id = id;
        this.abre = abre;
        this.cierra = cierra;
        this.fechaAbre = fAbre;
        this.fechaCierra = fCierra;
        READONLY = true;
    }
    
    public boolean abrirCaja(){
        if(READONLY)
            return false;
        
        String Query = "Select * from Caja where cierra =?";
        SenPreparada sen = new SenPreparada( conexion );
        sen.preparar(Query);
        
        if(sen.aI(1,CONSTANTES.CIERRADEFAULT).ejecutarSentencia()){
            try {
                if(sen.getRS().next()){
                    ResultSet res = sen.getRS();
                    id = res.getInt("caja.id");
                    abre = res.getInt("caja.abre");
                    fechaAbre = res.getTimestamp("caja.fechaabre");
                    cierra = CONSTANTES.CIERRADEFAULT;
                    return true;
                }else{
                    Optional<String> result = null;
                    TextInputDialog dialog = newTextInputCss();
                    dialog.setTitle("Abrir caja");
                    dialog.setHeaderText("¿Con cuánto dinero abrirá la caja?");
                    dialog.setContentText("Dinero");
                    
                   do{
                        sen.preparar("Insert into Caja(abre,fechaAbre) VALUES (?,now())");
                        dialog.getEditor().setText("");
                        dialog.getEditor().setOnKeyTyped((evt)->{
                            if(!evt.getCharacter().matches("([0-9])+"))evt.consume();
                        });


                        result = dialog.showAndWait();
                    
                        if( result.isPresent() && result.get().matches("([0-9])+") ){
                                String dinero = result.get();
                                try {
                                    if( sen.aI(1, dinero).actualizar() && sen.getRS().next() ){
                                        id = sen.getRS().getInt(1);
                                        SenPreparada InsertedCaja = new SenPreparada(conexion);
                                        InsertedCaja.preparar("Select *  from caja where id = ?");
                                        if(InsertedCaja.aI(1, id).ejecutarSentencia() && InsertedCaja.getRS().next() ){
                                            abre = InsertedCaja.getRS().getInt("abre");
                                            fechaAbre = InsertedCaja.getRS().getTimestamp("fechaabre");
                                            return true;
                                        }
                                    }
                                } catch (SQLException ex) {
                                    Logger.getLogger(Caja.class.getName()).log(Level.SEVERE, null, ex);
                                }

                        }
                    }while(result.isPresent());
                }
            } catch (SQLException ex) {
                Logger.getLogger(Caja.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        return false;
    }
    
    public int calcularDineroEnCaja(){
        if(READONLY)
            return -255;
        SenPreparada sen = new SenPreparada(conexion);
        sen.preparar("Select caja.id,caja.abre, (caja.abre+IFNULL(ing.monto,0)-IFNULL(ret.monto,0)+IFNULL(ventas.monto,0))as monto from caja left outer join ( Select caja_id,sum(monto)as monto from transaccioncaja where entrada = false group by caja_id )as ret on ret.caja_id = caja.id left outer join ( Select caja_id,sum(monto)as monto from transaccioncaja where entrada = true group by caja_id )as ing on ing.caja_id = caja.id left outer join (Select caja_id,det.monto from venta join (Select venta_id,sum(cantidad*precioventa)as monto from detventa group by venta_id)as det on det.venta_id = venta.id group by caja_id) as ventas on ventas.caja_id = caja.id where caja.id = ? LIMIT 1 ");
        try {
            if(sen.aI(1, getId() ).ejecutarSentencia() && sen.getRS().next() ){
                abre = sen.getRS().getInt("abre");
                cierra = sen.getRS().getInt("monto");
           }
        } catch (SQLException ex) {
            Logger.getLogger(CajaController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return -255;    
    }
    
    public int getId(){
        return id;
    }
    public int getAbre(){
        return abre;
    }
    
    public String getAbreGs(){
        return KFormat(abre)+"Gs.";
    }
    
    public String getCierraGs(){
        if(cierra == CONSTANTES.CIERRADEFAULT)
            return "Valor sin asignar";
        return KFormat(cierra)+"Gs.";
    }
    
    
    public String getFechaAbre(){
        return fechaAbre.toString();
    }
    
    public String getFechaCierra(){
        if(fechaCierra == null )
            return "Valor sin asignar.";
        return fechaCierra.toString();
    }
    
    public int getCierra(){
        return cierra;
    }
}

