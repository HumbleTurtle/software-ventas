/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FXML;

import FXML.secundarios.MarcaCat;
import FXML.secundarios.GvarController;
import FXML.secundarios.Helper.tClass;
import static FXML.secundarios.Static.Gen.newAlertCss;
import static FXML.secundarios.Static.Gen.newTextInputCss;
import static FXML.secundarios.Static.GenHelper.KFormat;
import clases.SenPreparada;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.property.SimpleStringProperty;
import static javafx.collections.FXCollections.observableArrayList;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextInputDialog;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import javafx.util.StringConverter;

/**
 *
 * @author Manabe
 */
public class InventarioController extends GvarController {
    @FXML TableView<Producto> tablaProductos;
    @FXML ComboBox<MarcaCat> comboMarca,comboCategoria;

    TableColumn nombrecol,preciocol,codcol,marcacol,cantidadcol,categoriacol;
    
    ObservableList<Producto> Lista_productos;
    ObservableList<MarcaCat> Lista_marcas,Lista_categorias;
    
    BarCodeReader BarCode_Reader ;
    
    
    @FXML void borrarProducto() throws SQLException{
        if(tablaProductos.getSelectionModel().getSelectedItem() != null ){
            Alert confirmar = newAlertCss( Alert.AlertType.CONFIRMATION );
            confirmar.setTitle("Confirmar acción.");
            confirmar.setHeaderText("¿Está seguro de borrar este producto?.");
            confirmar.setContentText("Se eliminarán las promociones asociadas. Ésta accion no se podrá deshacer.");

            Optional<ButtonType> op = confirmar.showAndWait();

            if (op.get() == ButtonType.OK){
                Producto producto = tablaProductos.getSelectionModel().getSelectedItem();            

                SenPreparada query = smanager.newSenPreparada();
                if ( producto.getCantidad() == 0 ){
                    query.preparar("UPDATE producto set habilitado = 0 where id = ?");
                    if(query.aI(1, producto.getId()).actualizar()){
                        Alert dialog = newAlertCss(AlertType.INFORMATION);
                        dialog.setTitle("Borrado exitoso.");
                        dialog.setHeaderText("Se ha borrado exitosamente el producto.");
                        dialog.setContentText(null);
                        dialog.showAndWait();
                        actualizarVentana();
                        query.preparar("Update promocion set habilitado = 0 where producto_id = ?");
                        query.aI(1,producto.getId()).actualizar();
                        
                    }

                }else{
                    Alert dialog = newAlertCss(AlertType.ERROR);
                    dialog.setTitle("Error.");
                    dialog.setHeaderText("El producto no puede ser borrado porque está en uso. Debe tener cantidad 0.");
                    dialog.setContentText(null);
                    dialog.showAndWait();
                    actualizarVentana();
                }
            }
        }
    }
    
    @FXML void actualizarStock() throws IOException{
        
        Producto producto = tablaProductos.getSelectionModel().getSelectedItem();
        if( producto != null && producto.getControlarStock() ){
            
            Map<String,tClass> prms = new HashMap<>();

            
            prms.put("id-producto", new tClass(producto.id) );
            
            Stage ventana = smanager.mostrarParam(smanager.ACTUALIZAR_STOCK, prms);
            
            if(ventana != null){
                ventana.showAndWait();
                MarcaCat c = null,m = null;
                if(comboCategoria.getSelectionModel().getSelectedIndex() != 0 )
                    c = comboCategoria.getSelectionModel().getSelectedItem();

                if(comboMarca.getSelectionModel().getSelectedIndex() != 0 )    
                    m = comboMarca.getSelectionModel().getSelectedItem();
                CargarProductos(m,c);
            }   
            
            
        }

    }
    @FXML void MostrarNuevoProducto() throws IOException{
        Stage ventana = smanager.mostrar(smanager.NUEVO_PRODUCTO);
        if(ventana != null){
            ventana.setOnHidden((evt)->{
                CargarMarcas();
                CargarCategorias();
                CargarProductos(null,null);
            });
            ventana.show();
        }
    }
    @FXML void actualizarVentana(){

        MarcaCat m=null,c=null;
        if(comboCategoria.getSelectionModel().getSelectedIndex() != 0 )
            c = comboCategoria.getSelectionModel().getSelectedItem();

        if(comboMarca.getSelectionModel().getSelectedIndex() != 0 )    
            m = comboMarca.getSelectionModel().getSelectedItem();
        CargarProductos(m,c);
        CargarMarcas();
        CargarCategorias();
    }
    @FXML 
    public void EditarProducto() throws IOException{
        if(tablaProductos.getSelectionModel().getSelectedItem() != null){
            int id = tablaProductos.getSelectionModel().getSelectedItem().getId();
            Map<String,tClass> params = new HashMap<>();
            tClass idProducto = new tClass();
            idProducto.set(id);
            params.put("idProducto",idProducto);
            Stage ventana = smanager.mostrarParam(smanager.EDITAR_PRODUCTO, params);
            if(ventana != null){
                ventana.setOnHidden((evt) ->{            
                    MarcaCat m=null,c=null;
                    if(comboCategoria.getSelectionModel().getSelectedIndex() != 0 )
                        c = comboCategoria.getSelectionModel().getSelectedItem();

                    if(comboMarca.getSelectionModel().getSelectedIndex() != 0 )    
                        m = comboMarca.getSelectionModel().getSelectedItem();
                    CargarProductos(m,c);
                });
                ventana.show();
            }
        }

    }
    @Override
    public void afterInit(){
            CargarMarcas();
            CargarCategorias();
            CargarProductos(null,null);
            
            BarCode_Reader= new BarCodeReader();
            getStage().toFront();
            tablaProductos.requestFocus();
            getStage().getScene().setOnKeyReleased((evt)->{
                BarCode_Reader.typed(evt);
            });
            BarCode_Reader.addListener((str)->{
                BuscarPorCod(str);
            });
            

    }
    
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        Lista_productos = observableArrayList();
        Lista_marcas = observableArrayList();
        Lista_categorias = observableArrayList(); 
        
        
        
        tablaProductos.setPlaceholder(new Label("No se ha agregado ningún producto"));

        nombrecol = new TableColumn("Nombre y descripción");        
        nombrecol.setCellValueFactory(new PropertyValueFactory<>("nombre"));
        
        codcol = new TableColumn("Código");
        codcol.setCellValueFactory(new PropertyValueFactory<>("cod"));
        
        preciocol = new TableColumn("Precio");        
        preciocol.setCellValueFactory(new PropertyValueFactory<>("precioun"));        
        
        marcacol = new TableColumn("Marca");
        marcacol.setCellValueFactory(new PropertyValueFactory<>("marca"));
        
        cantidadcol = new TableColumn("En Stock");
        cantidadcol.setCellValueFactory(new PropertyValueFactory<>("cantidad"));
        
        categoriacol = new TableColumn("Categoria/Tipo");
        categoriacol.setCellValueFactory(new PropertyValueFactory<>("categoria"));
        
        tablaProductos.getColumns().addAll(codcol,nombrecol,cantidadcol,marcacol,categoriacol,preciocol);
        
        
        tablaProductos.setItems(Lista_productos);        
        comboMarca.setItems(Lista_marcas);
        comboCategoria.setItems(Lista_categorias);
        
        tablaProductos.setEditable(false);
        
        
        comboMarca.setCellFactory((combo)->{
             return new ListCell<MarcaCat>() {
                     @Override
                     protected void updateItem(MarcaCat item, boolean empty) {
                             super.updateItem(item, empty);

                             if (item == null || empty) {
                                     setText(null);
                             } else {
                                     setText(item.Nombre);
                             }
                     }
             };
        });

        comboMarca.setConverter(new StringConverter<MarcaCat>() {
             @Override
             public String toString(MarcaCat item) {
                     if (item == null) {
                             return null;
                     } else {
                             return item.Nombre;
                     }
             }

             @Override
             public MarcaCat fromString(String personString) {
                     return null; // No conversion fromString needed.
             }
         });
        
        comboCategoria.setCellFactory((combo)->{
             return new ListCell<MarcaCat>() {
                     @Override
                     protected void updateItem(MarcaCat item, boolean empty) {
                             super.updateItem(item, empty);

                             if (item == null || empty) {
                                     setText(null);
                             } else {
                                     setText(item.Nombre);
                             }
                     }
             };
        });

        comboCategoria.setConverter(new StringConverter<MarcaCat>() {
             @Override
             public String toString(MarcaCat item) {
                     if (item == null) {
                             return null;                             
                     } else {
                             return item.Nombre;
                     }
             }

             @Override
             public MarcaCat fromString(String personString) {
                     return null; // No conversion fromString needed.
             }
         });
        
        /**************Evento selección marca ****************/
        comboMarca.setOnAction((evt)->{
            MarcaCat m=null,c=null;
            if(comboCategoria.getSelectionModel().getSelectedIndex() != 0 )
                c = comboCategoria.getSelectionModel().getSelectedItem();
            
            if(comboMarca.getSelectionModel().getSelectedIndex() != 0 )    
                m = comboMarca.getSelectionModel().getSelectedItem();
            CargarProductos(m,c);
        });
        
        comboCategoria.setOnAction((evt)->{   
            MarcaCat m=null,c=null;
            if(comboCategoria.getSelectionModel().getSelectedIndex() != 0 )
                c = comboCategoria.getSelectionModel().getSelectedItem();
            
            if(comboMarca.getSelectionModel().getSelectedIndex() != 0 )    
                m = comboMarca.getSelectionModel().getSelectedItem();
            CargarProductos(m,c);
            CargarMarcas();
        });
        

    }
    public void BuscarPorCod(String str){
            String Query= "Select * from producto inner join marca on marca.id=producto.marca_id inner join categoria on categoria.id = marca.categoria_id where producto.habilitado = 1 and codigo =? LIMIT 1";
                    
            if( str.matches("^(FFA)([1-9])+") ){
                Query= "Select * from producto inner join marca on marca.id=producto.marca_id inner join categoria on categoria.id = marca.categoria_id where producto.habilitado = 1 and producto.id =? LIMIT 1";      
                str = str.replace("FFA", "");
            }
                
            SenPreparada s = smanager.newSenPreparada();
            if ( s.preparar(Query) ){
                try {
                    String nombre,marca,cod,categoria;
                    int id,cant,precio; boolean controlar;
                    if(s.aS(1, str).ejecutarSentencia() && s.getRS().next()  ){
                        Lista_productos.clear();
                        
                        id = s.getRS().getInt("producto.id");
                        nombre = s.getRS().getString("producto.nombredesc");
                        marca = s.getRS().getString("marca.nombre");
                        cod = s.getRS().getString("producto.codigo");
                        precio = s.getRS().getInt("producto.precio");                   
                        cant = s.getRS().getInt("producto.cantidad");
                        categoria = s.getRS().getString("categoria.nombre");
                        controlar = s.getRS().getBoolean("producto.controlarstock");
                        Producto prod =new Producto(id,nombre,precio,marca,cod,cant,categoria);
                        prod.setControlarStock(controlar);

                        Lista_productos.add(prod);       
                    }
                } catch (SQLException ex) {
                    Logger.getLogger(InventarioController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            
    }
    public void BuscarPorCod(){
        
        TextInputDialog dialog = newTextInputCss("codigo");
        dialog.setTitle("Buscar producto por código");
        dialog.setHeaderText("Inserte el código del producto a buscar");
        dialog.setContentText("Código:");

        /**Respuesta**/
        Optional<String> result = dialog.showAndWait();
        result.ifPresent(codigo -> {
            BuscarPorCod(codigo);            
        });
    }
    
    public void CargarMarcas(){
        if(comboCategoria.getSelectionModel().getSelectedIndex() != -1){
            MarcaCat categoria = null;
            SenPreparada s = smanager.newSenPreparada();
            
            categoria = comboCategoria.getSelectionModel().getSelectedItem();
            String Query = "Select * from Marca where marca.habilitado = 1 and Marca.categoria_id = ?";
            s.preparar(Query);
            s.aI(1, categoria.Id).ejecutarSentencia();

            Lista_marcas.clear();
            try {
                Lista_marcas.add(new MarcaCat(0,""));
                while(s.getRS().next()){
                    String marca_nombre = s.getRS().getString("marca.nombre");
                    int marca_id = s.getRS().getInt("marca.id");
                    Lista_marcas.add(new MarcaCat(marca_id,marca_nombre));
                }
            } catch (SQLException ex) {
                Logger.getLogger(InventarioController.class.getName()).log(Level.SEVERE, null, ex);
            }        
        }
        

    }
    public void CargarCategorias(){
        String Query = "Select * from Categoria where habilitado = 1";
        SenPreparada s = smanager.newSenPreparada();
        s.preparar(Query);
        s.ejecutarSentencia();
        Lista_categorias.clear();
        try {
            Lista_categorias.add(new MarcaCat(0,""));
            while(s.getRS().next()){
                String marca_nombre = s.getRS().getString("categoria.nombre");
                int marca_id = s.getRS().getInt("categoria.id");
                Lista_categorias.add(new MarcaCat(marca_id,marca_nombre));
            }
        } catch (SQLException ex) {
            Logger.getLogger(InventarioController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void CargarProductos(MarcaCat Par_Marca,MarcaCat Par_Categoria){
        String nombre,marca,cod,categoria,Query = null;
        int id,precio,cant; boolean controlar;
        SenPreparada s = smanager.newSenPreparada();
            if(Par_Marca != null){
                Query= "Select * from marca join (Select * from producto where habilitado = 1) as producto on producto.marca_id = marca.id join categoria on marca.categoria_id = categoria.id where marca.id = ?";
                s.preparar(Query);
                s.aI(1, Par_Marca.Id);
             
            }else if(Par_Categoria != null){
                Query = "SELECT * FROM producto left join marca on marca.id = producto.marca_id left join categoria on marca.categoria_id = categoria.id where producto.habilitado = 1 and categoria.id = ?";
                s.preparar(Query);
                s.aI(1, Par_Categoria.Id);
            
            }else{
                Query = "Select * from producto,marca,categoria where producto.habilitado = 1 and marca.id = producto.marca_id and categoria.id=marca.categoria_id";
                s.preparar(Query);
            }

  
        if ( s.ejecutarSentencia() ){
           Lista_productos.clear();
            try {
                while(s.getRS().next()){
                    id = s.getRS().getInt("producto.id");
                    nombre = s.getRS().getString("producto.nombredesc");
                    marca = s.getRS().getString("marca.nombre");
                    cod = s.getRS().getString("producto.codigo");
                    precio = s.getRS().getInt("producto.precio");                   
                    cant = s.getRS().getInt("producto.cantidad");
                    categoria = s.getRS().getString("categoria.nombre");
                    controlar = s.getRS().getBoolean("producto.controlarstock");
                    Producto prod =new Producto(id,nombre,precio,marca,cod,cant,categoria);
                    prod.setControlarStock(controlar);
                    Lista_productos.add(prod);       
                }
            } catch (SQLException ex) {
                Logger.getLogger(InventarioController.class.getName()).log(Level.SEVERE, null, ex);
            }
       }
       
    }
    
    
    public static class Producto{
        protected final Number id,cantidad,vprecioun;
        protected final SimpleStringProperty nombre;
        protected final SimpleStringProperty marca;
        protected final SimpleStringProperty cod;
        protected final SimpleStringProperty categoria;
        protected final SimpleStringProperty precioun;
        protected boolean ControlarStock;
        
        
        Producto(int id,String lnombre,int lprecio,String marca,String cod,int cant,String categoria){
            this.id = id;
            this.nombre=new SimpleStringProperty(lnombre.substring(0,1).toUpperCase()+lnombre.substring(1));
            this.precioun=new SimpleStringProperty();
            this.vprecioun = lprecio;
            this.cod = new SimpleStringProperty(cod);
            this.marca = new SimpleStringProperty(marca.substring(0,1).toUpperCase()+marca.substring(1));
            this.cantidad = cant;
            this.categoria = new SimpleStringProperty(categoria.substring(0,1).toUpperCase()+categoria.substring(1));
        }
        
        public void setControlarStock(boolean b){
            ControlarStock = b;
        }
        
        public boolean getControlarStock(){
            return ControlarStock;
        }
        
        public int getId(){
            return id.intValue();
        }
        
        public String getCategoria(){
            return categoria.get();
        }
        public void setCategoria(String s){
            categoria.set(s);
        }
        
        public int getCantidad(){
            return cantidad.intValue();
        }

        
        public String getNombre(){
            return nombre.get();
        }
        public void setNombre(String s){
            nombre.set(s);
        }
        
        public String getPrecioun(){
            precioun.set(KFormat(vprecioun.intValue())+"Gs." );
            return precioun.get();
        }

        public String getCod(){
            if(cod.get() == null)
                return "FFA"+id.toString();
            return cod.get();
        }
        
        public void setCod(String s){
            cod.set(s);
        }
        public String getMarca(){
            return marca.get();
        }
        
        public void setMarca(String s){
            marca.set(s);
        }
    }
}
