/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FXML;

import FXML.secundarios.Caja;
import FXML.secundarios.GvarController;
import static FXML.secundarios.Static.Gen.newAlertCss;
import static FXML.secundarios.Static.Gen.newTextInputCss;
import static FXML.secundarios.Static.GenHelper.KFormat;
import static FXML.secundarios.Static.GenHelper.newCol;
import FXML.secundarios.TransaccionCaja;
import clases.SenPreparada;
import java.net.URL;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import static javafx.collections.FXCollections.observableArrayList;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.TableView;
import javafx.scene.control.TextInputDialog;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;

/**
 *
 * @author Manabe
 */
public class CajaController extends GvarController{
    @FXML Label campoAbreCaja, campoCierraCaja;
    @FXML TableView<TransaccionCaja> tablaTransacciones;

    ObservableList<TransaccionCaja> Lista_Transacciones;
    
    @FXML void calcularDineroActual(){
        campoAbreCaja.setText(KFormat( smanager.getAbreCaja() )+"Gs.");
        campoCierraCaja.setText(KFormat( smanager.getCierraCaja() ) +"Gs.");
    }
    
    @FXML void cerrarCaja() throws SQLException{
        Alert confirmar = newAlertCss( AlertType.CONFIRMATION );
        confirmar.setTitle("Confirmar acción.");
        confirmar.setHeaderText("¿Está seguro de cerrar la caja?.");
        confirmar.setContentText(null);
        
        Optional<ButtonType> op = confirmar.showAndWait();
        
        if (op.get() == ButtonType.OK){
            SenPreparada sen = smanager.newSenPreparada();
            sen.preparar("Update caja set cierra = ?,fechacierra = now() where id = ?");
            if( sen.aI(1, smanager.getCierraCaja()).aI(2,smanager.getIdCaja()).actualizar() ){
                Alert msg = newAlertCss(AlertType.INFORMATION);
                msg.setTitle("Caja cerrada.");
                msg.setHeaderText("La caja se ha cerrado correctamente. El programa se reiniciará.");
                msg.setContentText(null);
                msg.showAndWait();
                smanager.reiniciando(true);
                getStage().hide();
                smanager.reiniciar();
            }
        }        
    }
    
    @FXML void ingresarDinero() throws SQLException{
        SenPreparada sen = smanager.newSenPreparada();
        Optional<String> result = null;
        TextInputDialog dialog = newTextInputCss();
        dialog.setTitle("Ingresar dinero");
        dialog.setHeaderText("¿Cuánto dinero ingresará a la caja?");
        dialog.setContentText("Monto:");

       do{
            sen.preparar("Insert into TransaccionCaja(monto,entrada,caja_id,Usuario_id) VALUES (?,?,?,?)");
            dialog.getEditor().setText("");
            dialog.getEditor().setOnKeyTyped((evt)->{
                if(!evt.getCharacter().matches("([0-9])+"))evt.consume();
            });


            result = dialog.showAndWait();

            if( result.isPresent() && result.get().matches("([0-9])+") ){
                String dinero = result.get();
                    try {
                        if( sen.aI(1, dinero).aB(2, true).aI(3, smanager.getIdCaja()).aI(4, smanager.getIdUser()).actualizar() && sen.getRS().next() ){
                            Alert msg = newAlertCss(AlertType.INFORMATION);
                            msg.setTitle("Transaccion realizada con éxito.");
                            msg.setHeaderText("Se ha completado la transacción correctamente.");
                            msg.setContentText(null);
                            msg.showAndWait();

                        }
                    } catch (SQLException ex) {
                        Logger.getLogger(CajaController.class.getName()).log(Level.SEVERE, null, ex);
                    }

                break;
            }
        }while(result.isPresent());
        cargarLista();
        calcularDineroActual();
    }
    @FXML void retirarDinero(){
        SenPreparada sen = smanager.newSenPreparada();
        Optional<String> result = null;
        TextInputDialog dialog = newTextInputCss();
        dialog.setTitle("Retirar dinero");
        dialog.setHeaderText("¿Cuánto dinero retirará de la caja?");
        dialog.setContentText("Monto:");

       do{
            sen.preparar("Insert into TransaccionCaja(monto,entrada,caja_id,usuario_id) VALUES (?,?,?,?)");
            dialog.getEditor().setText("");
            dialog.getEditor().setOnKeyTyped((evt)->{
                if(!evt.getCharacter().matches("([0-9])+"))evt.consume();
            });


            result = dialog.showAndWait();

            if( result.isPresent() && result.get().matches("([0-9])+") ){
                String dinero = result.get();
              if(Integer.parseInt(dinero) > 0 )
                if(Integer.parseInt(dinero) <= smanager.getCierraCaja()  ){
                    try {
                        if( sen.aI(1, dinero).aB(2, false).aI(3, smanager.getIdCaja()).aI(4, smanager.getIdUser()).actualizar() && sen.getRS().next() ){
                            Alert msg = newAlertCss(AlertType.INFORMATION);
                            msg.setTitle("Transaccion realizada con éxito.");
                            msg.setHeaderText("Se ha completado la transacción correctamente.");
                            msg.setContentText(null);
                            msg.showAndWait();

                        }
                    } catch (SQLException ex) {
                        Logger.getLogger(CajaController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                break;
                }else{
                    Alert msg = newAlertCss(AlertType.INFORMATION);
                    msg.setTitle("Dinero insuficiente.");
                    msg.setHeaderText("La caja no cuenta con esa cantidad de dinero.");
                    msg.setContentText(null);
                    msg.showAndWait();

                }
            }
        }while(result.isPresent());
        cargarLista();    
        calcularDineroActual();
    }

    
    private void cargarLista() {
        Lista_Transacciones.clear();
        SenPreparada sen = smanager.newSenPreparada();
        sen.preparar("Select * from transaccioncaja join usuario on usuario.id = transaccioncaja.usuario_id where caja_id = ? ORDER BY transaccioncaja.ID DESC");
        if(sen.aI(1,smanager.getIdCaja()).ejecutarSentencia()){
            try {
                while(sen.getRS().next()){
                    Lista_Transacciones.add(new TransaccionCaja(sen.getRS().getString("Usuario.nombre"),sen.getRS().getBoolean("entrada"),sen.getRS().getInt("monto")));
                }
            } catch (SQLException ex) {
                Logger.getLogger(CajaController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    @Override
    public void afterInit(){
        cargarLista();
    }
    
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        Lista_Transacciones = observableArrayList();
        tablaTransacciones.setPlaceholder(new Label("No se ha realizado ninguna transacción."));
        tablaTransacciones.getColumns().clear();
        tablaTransacciones.setItems(Lista_Transacciones);
        tablaTransacciones.getColumns().addAll(newCol("Tipo de transaccion","Entrada"),newCol("Usuario","Usuario"),newCol("Monto de la transacción","Monto"));
        

    }
    
}
