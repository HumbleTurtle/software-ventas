/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FXML;

import FXML.secundarios.GvarController;
import static FXML.secundarios.Static.Gen.newAlertCss;
import clases.SenPreparada;
import java.io.IOException;
import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

/**
 *
 * @author Manabe
 */
public class LoginController extends GvarController{

    private Drag drag;
    @FXML
    private Pane initpane;
    @FXML TextField campo_nombre;
    @FXML PasswordField campo_password;
    
    @FXML
    public void cerrarVentana(){
        getStage().close();
    }
    @FXML
    public void ingresar() throws IOException{
        String nombre, password;
        nombre = campo_nombre.getText();
        password = campo_password.getText();
        if(campo_nombre.getLength() > 3 && campo_password.getLength()> 5){
            
            if(smanager.checkLogin(nombre, password) ){
                if(smanager.abrirCaja()){
                        Stage ventana = smanager.mostrar(smanager.ROOTWINDOW);
                        if(ventana != null){
                            ventana.show();
                            getStage().close();
                            return;
                        }
                }
                    Alert dialog = newAlertCss(AlertType.ERROR);
                    dialog.setTitle("Error, dinero en caja no definido.");
                    dialog.setHeaderText("Debe ingresar con cuánto dinero abre la caja.");
                    dialog.setContentText(null);
                    dialog.showAndWait();
                    return;
            }
    
        }
        
        Alert dialog = newAlertCss(AlertType.ERROR);
        dialog.setTitle("Inicio de sesión erróneo.");
        dialog.setHeaderText("El nombre de usuario y/o contraseña es incorrecto");
        dialog.setContentText(null);
        dialog.showAndWait();            
        

    }
  
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        drag = new Drag();
        initpane.addEventFilter(MouseEvent.MOUSE_PRESSED, (MouseEvent evt)->{
            if(evt.getButton() == MouseButton.PRIMARY ){
                drag.iX=evt.getX();
                drag.iY=evt.getY();
            }
        });
        
        initpane.addEventFilter(MouseEvent.MOUSE_DRAGGED, (MouseEvent evt)->{
            if(evt.getButton()== MouseButton.PRIMARY){
                getStage().setX(evt.getScreenX()-drag.iX);
                getStage().setY(evt.getScreenY()-drag.iY);
            }
        });
        return;
    }

}
