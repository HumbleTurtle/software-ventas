/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FXML;

import FXML.secundarios.Venta;
import FXML.secundarios.GvarController;
import FXML.secundarios.Helper.tClass;
import static FXML.secundarios.Static.Func.sinSimbolos;
import static FXML.secundarios.Static.Gen.newTextInputCss;
import static FXML.secundarios.Static.GenHelper.newCol;
import clases.SenPreparada;
import java.io.IOException;

import java.net.URL;
import java.sql.Date;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.Instant;
import java.time.ZoneId;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import static javafx.collections.FXCollections.observableArrayList;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableView;
import javafx.scene.control.TextInputDialog;
import javafx.stage.Stage;

/**
 *
 * @author Manabe
 */
public class RegistroVentasController extends GvarController {
    @FXML TableView<Venta> tablaVentas;
    @FXML DatePicker campoFecha;
    ObservableList<Venta> Lista_Ventas;
    
    @FXML void mostrarVenta() throws IOException{
        if(tablaVentas.getSelectionModel().getSelectedItem() != null ){
            Map<String,tClass> prms = new HashMap<>();
            Venta venta = tablaVentas.getSelectionModel().getSelectedItem();
            prms.put("id-venta", new tClass( venta.getId() ) );
            
            Stage ventana = smanager.mostrarParam(smanager.VER_PRODUCTOS_VENTA, prms);
            if(ventana != null){
                ventana.showAndWait();
            }
        }
    }
    
    @Override
    public void afterInit(){
        defaultVentas();        
    }
    
    @FXML void filtrarPorFecha(){
        Lista_Ventas.clear();
        
        SenPreparada RegistroV = smanager.newSenPreparada();
        RegistroV.preparar("Select venta.id,venta.datetime, detven.valorventas from venta join (Select detventa.venta_id, sum(detventa.cantidad * detventa.precioventa )as valorventas from detventa group by venta_id ) as detven on venta.id = detven.venta_id where date(venta.datetime) = ? ORDER BY venta.id DESC");
        Date Fecha  = Date.valueOf(campoFecha.getValue());
        if(RegistroV.aD(1, Fecha ).ejecutarSentencia()){
            ResultSet rs = RegistroV.getRS();
            try {
                while(rs.next()){
                    Lista_Ventas.add(new Venta(rs.getInt("venta.id"),rs.getInt("valorventas"),rs.getDate("datetime")));
                }
            } catch (SQLException ex) {
                Logger.getLogger(RegistroVentasController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    @FXML void buscarPorCodigo(){
        
        Optional<String> result = null;
        TextInputDialog dialog = newTextInputCss();
        dialog.setTitle("Buscar venta por código.");
        dialog.setHeaderText("Ingrese el código/id de la venta");
        dialog.setContentText("Código/id:");
        SenPreparada sen = smanager.newSenPreparada();

        do{
            sen.preparar("Select venta.id,venta.datetime, detven.valorventas from venta join (Select detventa.venta_id, sum(detventa.cantidad * detventa.precioventa )as valorventas from detventa group by venta_id ) as detven on venta.id = detven.venta_id where venta.id=? ORDER BY venta.id DESC LIMIT 1");
            dialog.getEditor().setText("");
            dialog.getEditor().setOnKeyTyped((evt)->{
                if(!evt.getCharacter().matches("([0-9])+"))evt.consume();
            });

            result = dialog.showAndWait();

            if( result.isPresent() && result.get().matches("([0-9])+") ){
                result.ifPresent( (codigo)->{
                    try {
                        if(sen.aI(1, codigo).ejecutarSentencia() && sen.getRS().next()){
                            Lista_Ventas.clear();
                            Lista_Ventas.add(new Venta(sen.getRS().getInt("venta.id"),sen.getRS().getInt("valorventas"),sen.getRS().getDate("datetime")));
                        }
                    } catch (SQLException ex) {
                        Logger.getLogger(RegistroVentasController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                } );
            }
        }while(result.isPresent());
    }
    
    
    @FXML void defaultVentas(){
        Lista_Ventas.clear();
        SenPreparada RegistroV = smanager.newSenPreparada();
        RegistroV.preparar("Select venta.id,venta.datetime, detven.valorventas from venta join (Select detventa.venta_id, sum(detventa.cantidad * detventa.precioventa )as valorventas from detventa group by venta_id ) as detven on venta.id = detven.venta_id ORDER BY venta.id DESC");
        
        if(RegistroV.ejecutarSentencia()){
            ResultSet rs = RegistroV.getRS();
            try {
                while(rs.next()){
                    Lista_Ventas.add(new Venta(rs.getInt("venta.id"),rs.getInt("valorventas"),rs.getDate("datetime")));
                }
            } catch (SQLException ex) {
                Logger.getLogger(RegistroVentasController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        tablaVentas.getColumns().clear();
        
        Lista_Ventas = observableArrayList();

        
        tablaVentas.setItems(Lista_Ventas);
        tablaVentas.getColumns().addAll(newCol("ID Venta","Id"),newCol("Fecha de venta","Fecha"),newCol("Valor de la venta","ValorVenta"));
    }
    
}
