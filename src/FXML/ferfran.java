/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FXML;

import FXML.secundarios.GvarController;
import FXML.secundarios.StageManager;
import clases.SenPreparada;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Rectangle2D;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/**
 *
 * @author Manabe
 */
public class ferfran extends Application {
    
    @Override
    public void start(Stage stage) throws Exception {
        /* * Tamaño Mínimo de la ventana * */
        Double baseW,baseH;
        baseW=1024.0;
        baseH=768.0;
        
        Screen screen = Screen.getPrimary();
        Rectangle2D bounds = screen.getVisualBounds();
        
        StageManager Manager = new StageManager();
        Manager.setApp(this);
        
        FXMLLoader $principal=new FXMLLoader(getClass().getResource("fxml/iniciarsesion.fxml"));
        Parent _principal= (Parent)$principal.load();
        
        GvarController controller = $principal.<GvarController>getController();
        controller.smanager= Manager;
        controller.setStage(stage);



        
        Scene s_principal = new Scene(_principal);

        

        stage.initStyle(StageStyle.UNDECORATED);
        stage.setScene(s_principal);
        
        
        
        stage.sizeToScene();
        stage.show();
        stage.setResizable(false);
        stage.setTitle("Ingreso - Control de Stock");
        
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
       
    }
    @Override
    public void stop(){
        try {
            this.finalize();
        } catch (Throwable ex) {
            Logger.getLogger(ferfran.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}

class ${
    public static double calcularEscala(Double original, Double actual){
        Double escala = actual/original;
        if(escala<1)
            return 1;
        else
            
            return escala;
    }

}