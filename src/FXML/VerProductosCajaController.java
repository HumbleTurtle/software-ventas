/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FXML;

import FXML.FacturacionController.ProductoInv;
import FXML.InventarioController.Producto;
import FXML.secundarios.GvarController;
import FXML.secundarios.Static;
import FXML.secundarios.Static.CONSTANTES;
import static FXML.secundarios.Static.Gen.newAlertCss;
import static FXML.secundarios.Static.GenHelper.KFormat;
import static FXML.secundarios.Static.GenHelper.newCol;
import FXML.secundarios.TransaccionCaja;
import clases.SenPreparada;
import java.net.URL;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import static javafx.collections.FXCollections.observableArrayList;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.TableView;

/**
 *
 * @author Manabe
 */
public class VerProductosCajaController extends GvarController{
    
    @FXML TableView<VentaProducto> tablaVentas;
    @FXML TableView<TransaccionCaja> tablaTransacciones;
    
    @FXML Label campoIdCaja,campoFechaAbre,campoFechaCierra,campoDineroAbre,campoDineroCierra;
    
    ObservableList<VentaProducto> Lista_Productos ;
    ObservableList<TransaccionCaja> Lista_Transacciones;
    
    private int IdCaja,IdDetVenta,CajaCierra;
    
    @FXML void borrarDetVenta() throws SQLException{
        if(tablaVentas.getSelectionModel().getSelectedItem() != null){
            if(CajaCierra == CONSTANTES.CIERRADEFAULT ){
                Alert confirmar = newAlertCss( Alert.AlertType.CONFIRMATION );
                confirmar.setTitle("Confirmar acción.");
                confirmar.setHeaderText("¿Está seguro de querer borrar esta operacion de la venta?.");
                confirmar.setContentText(null);

                Optional<ButtonType> op = confirmar.showAndWait();

                if (op.get() == ButtonType.OK){
                    VentaProducto venta = tablaVentas.getSelectionModel().getSelectedItem();
                    SenPreparada sen = smanager.newSenPreparada();
                    sen.preparar("SELECT * FROM detpromo join detventa on detventa.id = ?");
                    if ( sen.aI(1, venta.getIdDetVenta() ).ejecutarSentencia() && sen.getRS().next() ){
                        SenPreparada actualizarExistencias = smanager.newSenPreparada();
                        actualizarExistencias.preparar("Update producto set cantidad= cantidad + ? where id = ? and controlarstock = 1");
                        if(actualizarExistencias.aI(1,sen.getRS().getInt("detpromo.cantidad")).aI(2, sen.getRS().getInt("detventa.producto_id")).actualizar() ){
                            SenPreparada eliminar = smanager.newSenPreparada();
                            eliminar.preparar("Delete detpromo from detpromo where detventa_id = ?");
                            if(eliminar.aI(1, venta.getIdDetVenta() ).actualizar()){
                                eliminar.preparar("Delete detventa from detventa where id = ?");
                                if(eliminar.aI(1, venta.getIdDetVenta() ).actualizar() ){
                                    cargarProductos();
                                    Alert dialog = newAlertCss(AlertType.INFORMATION);
                                    dialog.setTitle("Operacion realizada con éxito.");
                                    dialog.setHeaderText("Se ha eliminado correctamente la venta.");
                                    dialog.setContentText(null);
                                    dialog.showAndWait();
                                }
                            }
                        }
                    }else{
                        sen.preparar("Select * from detventa where id = ?");
                        if(sen.aI(1, venta.getIdDetVenta() ).ejecutarSentencia() && sen.getRS().next() ){
                            SenPreparada actualizarExistencias = smanager.newSenPreparada();
                            actualizarExistencias.preparar("Update producto set cantidad= cantidad + ? where id = ?");
                            if(actualizarExistencias.aI(1,sen.getRS().getInt("detventa.cantidad")).aI(2, sen.getRS().getInt("detventa.producto_id")).actualizar() ){
                                SenPreparada eliminar = smanager.newSenPreparada();
                                eliminar.preparar("Delete detpromo from detpromo where detventa_id = ?");
                                if(eliminar.aI(1, venta.getIdDetVenta() ).actualizar()){
                                    eliminar.preparar("Delete detventa from detventa where id = ?");
                                    if(eliminar.aI(1, venta.getIdDetVenta() ).actualizar() ){
                                        cargarProductos();
                                        Alert dialog = newAlertCss(AlertType.INFORMATION);
                                        dialog.setTitle("Operacion realizada con éxito.");
                                        dialog.setHeaderText("Se ha eliminado correctamente la venta.");
                                        dialog.setContentText(null);
                                        dialog.showAndWait();
                                    }
                                }
                            }
                        }
                    }                   

                }
            }else{
                Alert dialog = newAlertCss(AlertType.INFORMATION);
                dialog.setTitle("Esta caja ya se ha cerrado");
                dialog.setHeaderText("Esta caja ya se ha cerrado y no puede ser modificada.");
                dialog.setContentText(null);
                dialog.showAndWait();
            }
        }
    }
    
    void cargarProductos(){
        SenPreparada sen = smanager.newSenPreparada();
        sen.preparar("Select venta.id,detventa.id,detventa.cantidad,detventa.precioventa,coalesce(detpromo.nombredesc,producto.nombredesc) as nombreventa,venta.datetime from detventa join (Select id,caja_id,datetime from venta)as venta on venta.id = detventa.venta_id join producto on producto.id = detventa.producto_id left join detpromo on detpromo.detventa_id = detventa.id where venta.caja_id = ?");

        try {
            Lista_Productos.clear();
            if(sen.aI(1, IdCaja ).ejecutarSentencia() && sen.getRS().next() ){                    
                    Lista_Productos.add(new VentaProducto(sen.getRS().getInt("venta.id"),sen.getRS().getInt("detventa.id"),sen.getRS().getString("nombreventa"),sen.getRS().getInt("detventa.cantidad"),sen.getRS().getInt("detventa.precioventa"),sen.getRS().getTimestamp("datetime")));


                    while(sen.getRS().next()){
                        Lista_Productos.add(new VentaProducto(sen.getRS().getInt("venta.id"),sen.getRS().getInt("detventa.id"),sen.getRS().getString("nombreventa"),sen.getRS().getInt("detventa.cantidad"),sen.getRS().getInt("detventa.precioventa"),sen.getRS().getTimestamp("datetime")));
                    }

            }
        } catch (SQLException ex) {
            Logger.getLogger(VerProductosCajaController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    @Override
    public void afterInit(){
        if(params.get("id-caja") != null ){
            int idcaja = (int) params.get("id-caja").get();
            IdCaja = idcaja;
            
            SenPreparada sen = smanager.newSenPreparada();
            sen.preparar("Select * from caja where id = ?");
            
            try {
                if(sen.aI(1, idcaja).ejecutarSentencia() && sen.getRS().next()){
                    campoIdCaja.setText(sen.getRS().getString("caja.id"));
                    campoFechaAbre.setText(sen.getRS().getTimestamp("caja.fechaabre").toString());
                    
                    campoFechaCierra.setText((sen.getRS().getTimestamp("caja.fechacierra") != null ) ?  sen.getRS().getTimestamp("caja.fechacierra").toString() : "Valor sin asignar.");
                    campoDineroAbre.setText( KFormat( sen.getRS().getInt("caja.abre") ) + "Gs.");

                    CajaCierra = sen.getRS().getInt("caja.cierra");
                    if ( CajaCierra == CONSTANTES.CIERRADEFAULT )
                        campoDineroCierra.setText( "La caja continúa abierta");
                    else
                        campoDineroCierra.setText( KFormat( CajaCierra )+"Gs.");
                }
            } catch (SQLException ex) {
                Logger.getLogger(VerProductosCajaController.class.getName()).log(Level.SEVERE, null, ex);
            }

            
            cargarProductos();
        
            sen.preparar("Select * from transaccioncaja join usuario on usuario.id = transaccioncaja.usuario_id where caja_id = ? ORDER BY transaccioncaja.id DESC");
            if(sen.aI(1, idcaja).ejecutarSentencia()){
                try {
                    while(sen.getRS().next()){
                        Lista_Transacciones.add(new TransaccionCaja(sen.getRS().getString("usuario.nombre"),sen.getRS().getBoolean("entrada"),sen.getRS().getInt("monto")));
                    }
                } catch (SQLException ex) {
                    Logger.getLogger(VerProductosCajaController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }

        }else{
            getStage().setOnShown((evt)->{
                Salir();
            });
            
        }
        
    }
    
    @Override
    public void initialize(URL location, ResourceBundle resources) {
    Lista_Productos = observableArrayList();
    Lista_Transacciones = observableArrayList();
    
    tablaVentas.setPlaceholder(new Label("No se ha realizado ninguna venta en el periodo de esta caja."));
    tablaVentas.getColumns().clear();
    tablaVentas.setItems(Lista_Productos);
    tablaVentas.getColumns().addAll(newCol("ID Venta","IdVenta"),newCol("Nombre del producto","Nombre"),newCol("Cantidad vendida","CantidadVendida"),newCol("Precio Unitario","PrecioVenta"),newCol("Importe","Importe"));
    
    
    tablaTransacciones.setPlaceholder(new Label("No se ha realizado ninguna transacción en el periodo de esta caja."));
    tablaTransacciones.getColumns().clear();
    tablaTransacciones.setItems(Lista_Transacciones);
    tablaTransacciones.getColumns().addAll(newCol("Tipo de transaccion","Entrada"),newCol("Usuario","Usuario"),newCol("Monto de la transacción","Monto"));
    }
 
    public static class VentaProducto{
        private int IdVenta;
        private int IdDetVenta;
        private String Producto;
        private int PrecioVenta;
        private int CantidadVendida;
        private Timestamp Date;
        
        public VentaProducto(int idVenta,String producton,int cantidadv,int precioventa,Timestamp date){
            this.IdVenta = idVenta;
            this.PrecioVenta = precioventa;
            this.Producto = producton;
            this.CantidadVendida = cantidadv;
            this.Date = date;
            this.IdDetVenta = 0;
        }
        
        public VentaProducto(int idVenta,int detventaid,String producton,int cantidadv,int precioventa,Timestamp date){
            this.IdVenta = idVenta;
            this.PrecioVenta = precioventa;
            this.Producto = producton;
            this.CantidadVendida = cantidadv;
            this.Date = date;
            this.IdDetVenta = detventaid;
        }
        
        public String getNombre(){
            return Producto;
        }
        public String getFechaHora(){
            return Date.toString();
        }
        
        public int getIdVenta(){
            return IdVenta;
        }
        
        public int getIdDetVenta(){
            return IdDetVenta;
        }
        
        public String getPrecioVenta(){
            return KFormat(PrecioVenta)+"Gs.";
        }
        public int getCantidadVendida(){
            return CantidadVendida;
        }
 
        public String getImporte(){
            return KFormat(CantidadVendida*PrecioVenta)+"Gs.";
        }
    
    }
}
