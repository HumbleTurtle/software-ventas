/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FXML;

import FXML.InventarioController.Producto;
import FXML.secundarios.GvarController;
import FXML.secundarios.Helper.tClass;
import FXML.secundarios.StageE;
import static FXML.secundarios.Static.Func.sinSimbolos;
import static FXML.secundarios.Static.Gen.newAlertCss;
import static FXML.secundarios.Static.Gen.newTextInputCss;
import static FXML.secundarios.Static.GenHelper.Capitalize;
import static FXML.secundarios.Static.GenHelper.KFormat;
import clases.SenPreparada;
import java.io.IOException;

import java.util.Optional;

import java.net.URL;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.Observable;
import static javafx.collections.FXCollections.observableArrayList;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;

import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextInputDialog;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

/**
 *
 * @author Manabe
 */
public class FacturacionController extends GvarController{

    @FXML private String msg;
    @FXML private TableView<ProductoInv> tablaVentas = new TableView<>();
    @FXML private ObservableList<ProductoInv> Lista_Productos= observableArrayList();


    private TableColumn nombrecol,preciouncol,cantidadcol,importecol;
    
    @FXML private AnchorPane anchorTablaFactura;
    @FXML Label CampoCantidadCobrar;
    
    @FXML private MenuItem cuentas;
    
    private BarCodeReader BarCode_Reader;
    private int APagarv,VentaId;

    private Map<Integer,Integer> ProductosCargados;
    
    void calcularVuelto(){
        Optional<String> dinero = null;
        TextInputDialog dialog = newTextInputCss();
        dialog.setTitle("Calcular vuelto.");
        dialog.setHeaderText("Dinero que abona el cliente.");
        dialog.setContentText("Dinero:");
        int vdinero;
        
        do{
            vdinero = 999999999;
            dialog.getEditor().setText("");
            dialog.getEditor().setOnKeyTyped((evt)->{
                if(!evt.getCharacter().matches("([0-9])+"))evt.consume();
            });

            dinero = dialog.showAndWait();
            if(dinero.isPresent() && dinero.get().matches("([0-9])+") ){
                 vdinero = Integer.parseInt(dinero.get());
                 if(vdinero >= APagarv){
                    Alert msg = newAlertCss(AlertType.INFORMATION);
                    msg.setTitle("Vuelto");
                    msg.setHeaderText("Guaraníes: "+KFormat(vdinero)+"Gs. Vuelto: "+ KFormat(vdinero-APagarv)+"Gs." );
                    msg.setContentText(null);
                    msg.showAndWait();
                    break;
                 }else{
                    Alert msg = newAlertCss(AlertType.ERROR);
                    msg.setTitle("Vuelto");
                    msg.setHeaderText("El importe abonado es menor al total a pagar.");
                    msg.setContentText(null);
                    msg.showAndWait();
                 }

                 
            }

        }while(dinero.isPresent() || (!dinero.isPresent() && vdinero < APagarv));
    }
    
    @FXML void Guardar() throws SQLException{
        if(Lista_Productos.size() > 0 ){
            SenPreparada InsertVenta = smanager.newSenPreparada();
            InsertVenta.preparar("Insert into venta(Datetime,Usuario_id,Caja_id)values(now(),?,?)");
            if ( InsertVenta.aI(1,smanager.getIdUser() ).aI(2,smanager.getIdCaja()).actualizar() && InsertVenta.getRS().next() ){
                VentaId = InsertVenta.getRS().getInt(1);
            }
            
            SenPreparada InsertDetVenta = smanager.newSenPreparada();
            InsertDetVenta.preparar("Insert into detventa(venta_id,producto_id,cantidad,precioventa) values (?,?,?,?)");

            for( ProductoInv producto : Lista_Productos ){
                if(InsertDetVenta.aI(2, producto.getId()).aI(1, VentaId).aI(3, producto.isPromocion() ? 1 :producto.getCantidad()).aI(4, producto.getPreciov()).actualizar() && 
                        InsertDetVenta.getRS().next() && producto.getControlarStock() ){
                            
                            SenPreparada InsertDetPromo = smanager.newSenPreparada();
                            InsertDetPromo.preparar("Insert into detpromo(nombredesc,detventa_id,cantidad) values (?,?,?)");
                            
                            SenPreparada DisminuirExistencias = smanager.newSenPreparada();
                            DisminuirExistencias.preparar("Update producto set cantidad = (cantidad - ?) where id = ?");
                            if(DisminuirExistencias.aI(1, producto.getCantidad()).aI(2, producto.getId()).actualizar()){
                                if( producto.isPromocion() )
                                if( !InsertDetPromo.aS(1,producto.getNombre()).aI(2,InsertDetVenta.getRS().getInt(1)).aI(3,producto.getCantidad()).actualizar() ){
                                    Alert error = newAlertCss(AlertType.INFORMATION);
                                    error.setTitle("Error de Inserción");
                                    error.setHeaderText("Ha habido un error al procesar los datos.");
                                    error.setContentText(null);
                                    error.showAndWait();
                                }

                            }
                        }
            }
            calcularVuelto();
            Lista_Productos.clear();
            ProductosCargados.clear();

            Alert dialog = newAlertCss(AlertType.INFORMATION);
            dialog.setTitle("Guardado realizado.");
            dialog.setHeaderText("La venta se ha guardado correctamente.");
            dialog.setContentText(null);
            dialog.showAndWait();

            

        }else{
            Alert dialog = newAlertCss(AlertType.ERROR);
            dialog.setTitle("Error.");
            dialog.setHeaderText("Debe cargar algún producto para realizar la venta.");
            dialog.setContentText(null);
            dialog.showAndWait();
        }
        
    } 
    
    @FXML private void agregarElemento(ActionEvent event) {
        tablaVentas.setEditable(false);
    }
    
    @FXML private void seleccionarProducto() throws IOException{
    
        StageE ventana = smanager.mostrarSE(smanager.SELECCIONAR_PRODUCTO);
        if(ventana != null){
            ventana.stage.showAndWait();
            
            if ( ventana != null ){
                GvarController controller = ventana.fx.<GvarController>getController();
                if(controller.params.get("id-producto")!= null){
                    int nuevacant=0;
                    SenPreparada s = smanager.newSenPreparada();
                    if ( s.preparar("Select * from producto inner join marca on marca.id=producto.marca_id inner join categoria on categoria.id = marca.categoria_id where producto.id =? LIMIT 1") ){
                        try {
                            String nombre,marca,cod,categoria;
                            int id,cantidad,precio;
                            if( s.aS(1, ""+(int)controller.params.get("id-producto").get() ).ejecutarSentencia() && s.getRS().next() ){

                                id = s.getRS().getInt("producto.id");
                                nombre = s.getRS().getString("producto.nombredesc");
                                marca = s.getRS().getString("marca.nombre");
                                cod = s.getRS().getString("producto.codigo");
                                precio = s.getRS().getInt("producto.precio");                   
                                cantidad = s.getRS().getInt("producto.cantidad");
                                categoria = s.getRS().getString("categoria.nombre");
                                boolean controlarstock = s.getRS().getBoolean("producto.controlarstock");



                                if(cod == null)
                                    cod = s.getRS().getString("producto.id");

                                Optional<String> cantidadventa = null;
                                TextInputDialog dialog = newTextInputCss();
                                dialog.setTitle("Cantidad a vender");
                                dialog.setHeaderText("Ingrese la cantidad a vender");
                                dialog.setContentText("Cantidad");

                                do{

                                    dialog.getEditor().setText("");
                                    dialog.getEditor().setOnKeyTyped((evt)->{
                                        if(!evt.getCharacter().matches("([0-9])+"))evt.consume();
                                    });

                                    cantidadventa = dialog.showAndWait();
                                    if(cantidadventa.isPresent() && cantidadventa.get().matches("([0-9])+") && Integer.parseInt(cantidadventa.get()) > 0 ){
                                        int cantidadv = Integer.parseInt(cantidadventa.get());
                                        if(ProductosCargados.containsKey(id)){
                                            nuevacant = ProductosCargados.get(id)+ cantidadv;
                                             if( nuevacant <= cantidad  || !controlarstock ){
                                                ProductosCargados.replace(id, nuevacant );
                                                ProductoInv aux = new ProductoInv(id,nombre,precio,marca,cod,cantidad,categoria,Integer.parseInt(cantidadventa.get()));
                                                aux.setControlarStock(controlarstock);
                                                Lista_Productos.add(aux);
                                             }else{
                                                 /**Mensaje: Supera cantidad**/
                                                 Alert errormsg = newAlertCss(AlertType.ERROR);
                                                 errormsg.setTitle("Existencias insuficientes.");
                                                 errormsg.setHeaderText("Esa cantidad supera las existencias.");
                                                 errormsg.setContentText(null);
                                                 errormsg.showAndWait();
                                             }
                                        }else{
                                            nuevacant = cantidadv;
                                            if( nuevacant <= cantidad || !controlarstock ){
                                                ProductosCargados.put(id,nuevacant);
                                                ProductoInv aux = new ProductoInv(id,nombre,precio,marca,cod,cantidad,categoria,Integer.parseInt(cantidadventa.get()));
                                                aux.setControlarStock(controlarstock);
                                                Lista_Productos.add(aux);
                                            }else{
                                             /**Mensaje: Supera cantidad**/
                                                Alert errormsg = newAlertCss(AlertType.ERROR);
                                                errormsg.setTitle("Existencias insuficientes.");
                                                errormsg.setHeaderText("Esa cantidad supera las existencias.");
                                                errormsg.setContentText(null);
                                                errormsg.showAndWait();
                                            }
                                        }

                                    }
                                         break;



                                }while(cantidadventa.isPresent());

                            }       

                        } catch (SQLException ex) {
                            Logger.getLogger(InventarioController.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    
                    }
                }
            }
        }
    }
    
    public void CalcularTotalAPagar(){
        APagarv = 0;
        Lista_Productos.stream().forEach((s)->{
            APagarv+=s.getImportev();
        });
        CampoCantidadCobrar.setText(KFormat(APagarv)+" Gs.");
    }
    
    @FXML void BorrarSel(){
        if(tablaVentas.getSelectionModel().getSelectedItem() != null ){
            
            ProductoInv producto = tablaVentas.getSelectionModel().getSelectedItem();
            if(ProductosCargados.containsKey(producto.getId()) && ProductosCargados.get( producto.id ) == producto.getCantidadv()  ){
                ProductosCargados.remove(producto.getId());
            }else{
                ProductosCargados.replace(producto.getId(), ProductosCargados.get(producto.getId())-producto.getCantidadv());            
            }
            
            
            Lista_Productos.remove(producto);
        }
    }

    @FXML void BuscarPorCodBtn(){
        TextInputDialog dialog = newTextInputCss();
        dialog.setTitle("Insertar producto a la venta");
        dialog.setHeaderText("Ingrese el código del producto a vender");
        dialog.setContentText("Código:");
        
        
        Optional<String> codigo = null;
        do{
            codigo = dialog.showAndWait();
            
            dialog.getEditor().setOnKeyTyped((evt)->{
                  if( !sinSimbolos(evt.getCharacter()) )evt.consume();
            });
            
            if( codigo.isPresent() && !codigo.get().isEmpty() ){
                if( sinSimbolos(codigo.get()) )
                    if( codigo.get().matches("^(FFA)([1-9])+") )
                        BuscarPorID( codigo.get().replace("FFA", "") );
                    else if( codigo.get().matches("^(PROM)([1-9])+") ){
                        BuscarPorIDPROM( codigo.get().replace("PROM", "")  );
                    }
                    else                        
                        BuscarPorCod( codigo.get() );
                    break;
            }

        }while( codigo.isPresent() );
    }
    public void BuscarPorIDPROM(String str){            
            int nuevacant=0;
            SenPreparada s = smanager.newSenPreparada();
            if ( s.preparar("Select * from promocion join producto on producto.id = promocion.producto_id inner join marca on marca.id=producto.marca_id inner join categoria on categoria.id = marca.categoria_id where promocion.id=? and promocion.habilitado = 1 GROUP BY PROMOCION.id LIMIT 1") ){
                try {
                    String nombre,marca,cod,categoria;
                    int id,cantidad,cantidadAVender,precio;
                    if( s.aS(1, str).ejecutarSentencia() && s.getRS().next() ){
                        
                        id = s.getRS().getInt("producto.id");
                        nombre ="[PROM]" + Capitalize(s.getRS().getString("promocion.nombredesc") );
                        marca = s.getRS().getString("marca.nombre");
                        cod = s.getRS().getString("producto.codigo");
                        precio = s.getRS().getInt("promocion.preciototal");                   
                        cantidad = s.getRS().getInt("producto.cantidad");
                        categoria = s.getRS().getString("categoria.nombre");
                        cantidadAVender = s.getRS().getInt("promocion.cantidad");
                        boolean controlarstock = s.getRS().getBoolean("producto.controlarstock");
                        

                        if(cod == null)
                            cod = s.getRS().getString("producto.id");
                      

                        if(ProductosCargados.containsKey(id)){
                            nuevacant = ProductosCargados.get(id)+ cantidadAVender;
                             if( nuevacant <= cantidad  || !controlarstock ){
                                ProductosCargados.replace(id, nuevacant );
                                ProductoInv aux = new ProductoInv(id,nombre,precio,marca,cod,cantidad,categoria,cantidadAVender);
                                aux.isPromocion(true);
                                aux.setControlarStock(controlarstock);
                                Lista_Productos.add(aux);
                             }else{
                                 /**Mensaje: Supera cantidad**/
                                 Alert errormsg = newAlertCss(AlertType.ERROR);
                                 errormsg.setTitle("Existencias insuficientes.");
                                 errormsg.setHeaderText("Esa cantidad supera las existencias.");
                                 errormsg.setContentText(null);
                                 errormsg.showAndWait();
                             }
                        }else{
                            nuevacant = cantidadAVender;
                            if( nuevacant <= cantidad || !controlarstock ){
                                ProductosCargados.put(id,nuevacant);
                                ProductoInv aux = new ProductoInv(id,nombre,precio,marca,cod,cantidad,categoria,cantidadAVender);
                                aux.isPromocion(true);
                                aux.setControlarStock(controlarstock);
                                Lista_Productos.add(aux);
                            }else{
                             /**Mensaje: Supera cantidad**/
                                Alert errormsg = newAlertCss(AlertType.ERROR);
                                errormsg.setTitle("Existencias insuficientes.");
                                errormsg.setHeaderText("Esa cantidad supera las existencias.");
                                errormsg.setContentText(null);
                                errormsg.showAndWait();
                            }
                        }

                    }
                } catch (SQLException ex) {
                    Logger.getLogger(InventarioController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
    }
    public void BuscarPorID(String str){
            
            int nuevacant=0;
            SenPreparada s = smanager.newSenPreparada();
            if ( s.preparar("Select * from producto inner join marca on marca.id=producto.marca_id inner join categoria on categoria.id = marca.categoria_id where producto.habilitado = 1 and producto.id =? LIMIT 1") ){
                try {
                    String nombre,marca,cod,categoria;
                    int id,cantidad,precio;
                    if( s.aS(1, str).ejecutarSentencia() && s.getRS().next() ){
                        
                        id = s.getRS().getInt("producto.id");
                        nombre = s.getRS().getString("producto.nombredesc");
                        marca = s.getRS().getString("marca.nombre");
                        cod = s.getRS().getString("producto.codigo");
                        precio = s.getRS().getInt("producto.precio");                   
                        cantidad = s.getRS().getInt("producto.cantidad");
                        categoria = s.getRS().getString("categoria.nombre");
                        boolean controlarstock = s.getRS().getBoolean("producto.controlarstock");
                        
                        
                        
                        if(cod == null)
                            cod = s.getRS().getString("producto.id");

                        Optional<String> cantidadventa = null;
                        TextInputDialog dialog = newTextInputCss();
                        dialog.setTitle("Cantidad a vender");
                        dialog.setHeaderText("Ingrese la cantidad a vender");
                        dialog.setContentText("Cantidad");

                        do{

                            dialog.getEditor().setText("");
                            dialog.getEditor().setOnKeyTyped((evt)->{
                                if(!evt.getCharacter().matches("([0-9])+"))evt.consume();
                            });

                            cantidadventa = dialog.showAndWait();
                            if(cantidadventa.isPresent() && cantidadventa.get().matches("([0-9])+") && Integer.parseInt(cantidadventa.get()) > 0 ){
                                int cantidadv = Integer.parseInt(cantidadventa.get());
                                if(ProductosCargados.containsKey(id)){
                                    nuevacant = ProductosCargados.get(id)+ cantidadv;
                                     if( nuevacant <= cantidad  || !controlarstock ){
                                        ProductosCargados.replace(id, nuevacant );
                                        ProductoInv aux = new ProductoInv(id,nombre,precio,marca,cod,cantidad,categoria,Integer.parseInt(cantidadventa.get()));
                                        aux.setControlarStock(controlarstock);
                                        Lista_Productos.add(aux);
                                     }else{
                                         /**Mensaje: Supera cantidad**/
                                         Alert errormsg = newAlertCss(AlertType.ERROR);
                                         errormsg.setTitle("Existencias insuficientes.");
                                         errormsg.setHeaderText("Esa cantidad supera las existencias.");
                                         errormsg.setContentText(null);
                                         errormsg.showAndWait();
                                     }
                                }else{
                                    nuevacant = cantidadv;
                                    if( nuevacant <= cantidad || !controlarstock ){
                                        ProductosCargados.put(id,nuevacant);
                                        ProductoInv aux = new ProductoInv(id,nombre,precio,marca,cod,cantidad,categoria,Integer.parseInt(cantidadventa.get()));
                                        aux.setControlarStock(controlarstock);
                                        Lista_Productos.add(aux);
                                    }else{
                                     /**Mensaje: Supera cantidad**/
                                        Alert errormsg = newAlertCss(AlertType.ERROR);
                                        errormsg.setTitle("Existencias insuficientes.");
                                        errormsg.setHeaderText("Esa cantidad supera las existencias.");
                                        errormsg.setContentText(null);
                                        errormsg.showAndWait();
                                    }
                                }
                                
                            }
                                 break;
                                
                            

                        }while(cantidadventa.isPresent());
                        
                    }       

                } catch (SQLException ex) {
                    Logger.getLogger(InventarioController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
    }
    
    public void BuscarPorCod(String str){
            
            int nuevacant=0;
            SenPreparada s = smanager.newSenPreparada();
            if ( s.preparar("Select * from producto inner join marca on marca.id=producto.marca_id inner join categoria on categoria.id = marca.categoria_id where producto.habilitado = 1 and codigo =? LIMIT 1") ){
                try {
                    String nombre,marca,cod,categoria;
                    int id,cantidad,precio;
                    if( s.aS(1, str).ejecutarSentencia() && s.getRS().next() ){
                        
                        id = s.getRS().getInt("producto.id");
                        nombre = s.getRS().getString("producto.nombredesc");
                        marca = s.getRS().getString("marca.nombre");
                        cod = s.getRS().getString("producto.codigo");
                        precio = s.getRS().getInt("producto.precio");                   
                        cantidad = s.getRS().getInt("producto.cantidad");
                        categoria = s.getRS().getString("categoria.nombre");
                        boolean controlarstock = s.getRS().getBoolean("producto.controlarstock");
                        
                        
                        
                        if(cod == null)
                            cod = s.getRS().getString("producto.id");

                        Optional<String> cantidadventa = null;
                        TextInputDialog dialog = newTextInputCss();
                        dialog.setTitle("Cantidad a vender");
                        dialog.setHeaderText("Ingrese la cantidad a vender");
                        dialog.setContentText("Cantidad");

                        do{

                            dialog.getEditor().setText("");
                            dialog.getEditor().setOnKeyTyped((evt)->{
                                if(!evt.getCharacter().matches("([0-9])+"))evt.consume();
                            });

                            cantidadventa = dialog.showAndWait();
                            if(cantidadventa.isPresent() && cantidadventa.get().matches("([0-9])+") && Integer.parseInt(cantidadventa.get()) > 0 ){
                                int cantidadv = Integer.parseInt(cantidadventa.get());
                                if(ProductosCargados.containsKey(id)){
                                    nuevacant = ProductosCargados.get(id)+ cantidadv;
                                     if( nuevacant <= cantidad  || !controlarstock ){
                                        ProductosCargados.replace(id, nuevacant );
                                        ProductoInv aux = new ProductoInv(id,nombre,precio,marca,cod,cantidad,categoria,Integer.parseInt(cantidadventa.get()));
                                        aux.setControlarStock(controlarstock);
                                        Lista_Productos.add(aux);
                                     }else{
                                         /**Mensaje: Supera cantidad**/
                                         Alert errormsg = newAlertCss(AlertType.ERROR);
                                         errormsg.setTitle("Existencias insuficientes.");
                                         errormsg.setHeaderText("Esa cantidad supera las existencias.");
                                         errormsg.setContentText(null);
                                         errormsg.showAndWait();
                                     }
                                }else{
                                    nuevacant = cantidadv;
                                    if( nuevacant <= cantidad || !controlarstock ){
                                        ProductosCargados.put(id,nuevacant);
                                        ProductoInv aux = new ProductoInv(id,nombre,precio,marca,cod,cantidad,categoria,Integer.parseInt(cantidadventa.get()));
                                        aux.setControlarStock(controlarstock);
                                        Lista_Productos.add(aux);
                                    }else{
                                     /**Mensaje: Supera cantidad**/
                                        Alert errormsg = newAlertCss(AlertType.ERROR);
                                        errormsg.setTitle("Existencias insuficientes.");
                                        errormsg.setHeaderText("Esa cantidad supera las existencias.");
                                        errormsg.setContentText(null);
                                        errormsg.showAndWait();
                                    }
                                }
                                
                            }
                                 break;
                                
                            

                        }while(cantidadventa.isPresent());
                        
                    }       

                } catch (SQLException ex) {
                    Logger.getLogger(InventarioController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
    }
    
    @Override 
    public void afterInit(){
        getStage().setOnHidden(((evt)->{
            try {
                if( !smanager.reiniciando())
                    smanager.closeall();
            } catch (Exception ex) {
                Logger.getLogger(FacturacionController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }));
      
            BarCode_Reader= new BarCodeReader();
            getStage().toFront();
            tablaVentas.requestFocus();
            getStage().getScene().setOnKeyReleased((evt)->{
                BarCode_Reader.typed(evt);
            });

            BarCode_Reader.addListener((str)->{
                BuscarPorCod(str);     
            });

            Lista_Productos.addListener((Observable ee)->{
                CalcularTotalAPagar(); 
            });
    }
    
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        ProductosCargados = new HashMap<>();

        tablaVentas.setPlaceholder(new Label("No se ha agregado ningún producto"));
        
        tablaVentas.getSelectionModel().selectedItemProperty().addListener((obs,old,neew)->{
           if(tablaVentas.getSelectionModel().getSelectedItem() != null){
                Producto prod = tablaVentas.getSelectionModel().getSelectedItem();
             }

        }); 
        /*INICIALIZA TABLA*/
        /*Crea y define un valor de cada elemento del objeto*/
        nombrecol = new TableColumn("Nombre Producto");        
        nombrecol.setCellValueFactory(new PropertyValueFactory<>("nombre"));
        
        preciouncol = new TableColumn("Precio Unitario");
        preciouncol.setCellValueFactory(new PropertyValueFactory<>("precioun"));
        
        cantidadcol = new TableColumn("Cantidad");        
        cantidadcol.setCellValueFactory(new PropertyValueFactory<>("cantidadv"));
        
        
        importecol = new TableColumn("Importe");
        importecol.setCellValueFactory(new PropertyValueFactory<>("importe"));
        
        resizeCols();
        
        /*Si el anchorpane se agranda, las cols también*/
     //   anchorTablaFactura.widthProperty().addListener((obs,old,neew)->{resizeCols(neew); tablaVentas.setMinWidth(anchorTablaFactura.getWidth());});
    
        
        
        /*Redefine el tamaño de las columnas*/
        resizeCols();
        
        /*Se carga a la tabla*/
        tablaVentas.setItems(Lista_Productos);        
        tablaVentas.getColumns().addAll(nombrecol,preciouncol,cantidadcol,importecol,new TableColumn(),new TableColumn());        
    }
    
    private void resizeCols(){
        nombrecol.setMinWidth(200);
        preciouncol.setMinWidth(100);
        cantidadcol.setMinWidth(50);
        importecol.setMinWidth(150);
    }
    private void resizeCols(Number val){
        nombrecol.setPrefWidth(val.doubleValue()*.4);
        preciouncol.setPrefWidth(val.doubleValue()*.2);
        cantidadcol.setPrefWidth(val.doubleValue()*.1);
        importecol.setPrefWidth(val.doubleValue()*.3);

    }
    
    public void mensaje(String cadena){
        this.msg=cadena;
    }
    
    public class ProductoInv extends Producto{
       private int cantidadv;
       private String importe;
       private boolean ControlarStock;
       private boolean Promocion;
       
       public ProductoInv(int id,String lnombre,int lprecio,String marca,String cod,int cant,String categoria,int cantidadventa){
           super(id,lnombre,lprecio,marca,cod,cant,categoria);
           cantidadv = cantidadventa;
       }
 
       public boolean isPromocion(){
           return Promocion;
       }
       
       public void isPromocion(boolean bool){
           Promocion = bool;
       }
       
       public void setControlarStock(boolean controlar){
           ControlarStock = controlar;
       }
       
       public boolean getControlarStock(){ return ControlarStock; }
       
       public String getImporte(){
           if(Promocion)
               importe = KFormat(vprecioun.intValue());
           else
               importe = KFormat(cantidadv*vprecioun.intValue());
           return importe;
       }
       public int getImportev(){
           if(Promocion)
               return vprecioun.intValue();
           else
               return cantidadv*vprecioun.intValue();
       }
       
       public int getPreciov(){
           return vprecioun.intValue();
       }
       public int getCantidad(){
           return cantidadv;
       }
       public int getCantidadv(){
           if(Promocion)
               return 1;
           return cantidadv;
       }
       
       public void setCantidadVenta(int c){
          cantidadv = c;
       }
       
   }
}
