/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FXML;

import FXML.secundarios.GvarController;
import FXML.secundarios.MarcaCat;
import static FXML.secundarios.Static.Func.sinEspaciosB;
import static FXML.secundarios.Static.Func.sinSimbolos;
import static FXML.secundarios.Static.Func.sinSimbolosEspaciosA;
import static FXML.secundarios.Static.Gen.newAlertCss;
import clases.SenPreparada;
import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;

/**
 *
 * @author Manabe
 */
public class EditarCategoriaController extends GvarController{
    
    @FXML TextField campoNombre;
    private MarcaCat Categoria;
    @FXML void Guardar(){
        if(sinEspaciosB(campoNombre.getText()) && sinSimbolos(campoNombre.getText())){
            SenPreparada sen = smanager.newSenPreparada();
            sen.preparar("Select * from categoria where habilitado = 1 and nombre = ?");
            
            try {
                if(sen.aS(1, campoNombre.getText() ).ejecutarSentencia() && !sen.getRS().next() ){
                    sen.preparar("Update categoria set nombre = ? where id = ?");
                    if(sen.aI(2, Categoria.Id).aS(1, campoNombre.getText().toLowerCase() ).actualizar()){
                        Alert dialog = newAlertCss(Alert.AlertType.INFORMATION);
                        dialog.setTitle("Cambio exitoso.");
                        dialog.setHeaderText("El password de la cuenta se ha cambiado exitosamente.");
                        dialog.setContentText(null);
                        dialog.showAndWait();
                        getStage().close();
                    }
                }else{
                    Alert dialog = newAlertCss(Alert.AlertType.ERROR);
                    dialog.setTitle("Error de llenado.");
                    dialog.setHeaderText("Es posible que esa categoría ya exista.");
                    dialog.setContentText(null);
                    dialog.showAndWait();
                }
            } catch (SQLException ex) {
                Logger.getLogger(EditarCategoriaController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }else{
            Alert dialog = newAlertCss(Alert.AlertType.ERROR);
            dialog.setTitle("Error de llenado.");
            dialog.setHeaderText("No puede utilizar dobles espacios o espacios iniciales y/o finales en el campo.");
            dialog.setContentText(null);
            dialog.showAndWait();
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
    }
    

    
    @Override
    public void afterInit(){
        if(params.get("id-cat") != null){
            SenPreparada sen = smanager.newSenPreparada();
            int idCat = (int)params.get("id-cat").get();
            sen.preparar("Select * from categoria where habilitado = 1 and id = ?");
            try {
                if(sen.aI(1,idCat).ejecutarSentencia() && sen.getRS().next()){
                    Categoria = new MarcaCat(idCat,sen.getRS().getString("nombre"));
                    campoNombre.setText(Categoria.getNombre());
                }
            } catch (SQLException ex) {
                Logger.getLogger(EditarCategoriaController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
}
