/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FXML;

import FXML.secundarios.GvarController;
 import static FXML.secundarios.Static.Func.noVacio;
import static FXML.secundarios.Static.Func.sinEspaciosA;
import static FXML.secundarios.Static.Gen.newAlertCss;
import clases.SenPreparada;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;

/**
 *
 * @author Manabe
 */
public class EditarCuentaController extends GvarController {
    @FXML Label campo_nombre;
    @FXML PasswordField campo_password;
    
    private String nombre;
    private int id;
    
    @FXML
    public void Guardar(){
        if(campo_password.getLength()>5 && noVacio(campo_password.getText()) && sinEspaciosA(campo_password.getText()) ){
            String Query = "Update Usuario set password  = ? where id = ?";
            SenPreparada sen = smanager.newSenPreparada();

            sen.preparar(Query);
            if(sen.aS(1,campo_password.getText()).aI(2,id).actualizar()){
                Alert dialog = newAlertCss(AlertType.INFORMATION);
                dialog.setTitle("Cambio exitoso.");
                dialog.setHeaderText("El password de la cuenta se ha cambiado exitosamente.");
                dialog.setContentText(null);
                dialog.showAndWait();
                getStage().close();
            }
        }else{
            Alert dialog = newAlertCss(AlertType.ERROR);
            dialog.setTitle("Error de llenado.");
            dialog.setHeaderText("No puede utilizar espacios en la contraseña y ella debe tener mas de 5 caractéres.");
            dialog.setContentText(null);
            dialog.showAndWait();
            getStage().close();
        }
    }
    
    @Override
    public void afterInit(){
        if(params != null ){
            nombre = (String) params.get("nombre").get();
            id = (int) params.get("id").get();
            campo_nombre.setText(nombre);
        }
    }
    @Override
    public void initialize(URL location, ResourceBundle resources) {
    }
    
}
