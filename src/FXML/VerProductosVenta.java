/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FXML;

import FXML.VerProductosCajaController.VentaProducto;
import FXML.secundarios.GvarController;
import static FXML.secundarios.Static.GenHelper.Capitalize;
import static FXML.secundarios.Static.GenHelper.KFormat;
import static FXML.secundarios.Static.GenHelper.newCol;
import clases.SenPreparada;
import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import static javafx.collections.FXCollections.observableArrayList;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TableView;

/**
 *
 * @author Manabe
 */
public class VerProductosVenta extends GvarController{
    @FXML TableView<VentaProducto> tablaVentas;
    @FXML Label campoIdVenta,campoFechaVenta,campoVendedor,campoMonto;
    
    ObservableList<VentaProducto> Lista_Productos;
    
    @Override
    public void afterInit(){
        if(params.get("id-venta") != null ){
            int idventa = (int) params.get("id-venta").get();
            SenPreparada sen = smanager.newSenPreparada();
            sen.preparar("Select venta.id,venta.datetime, detventa.monto,usuario.nombre from venta left outer join (Select venta_id,sum(precioventa*cantidad)as monto from detventa group by detventa.venta_id )as detventa on detventa.venta_id = venta.id join usuario on usuario.id = venta.usuario_id where venta.id = ? ");
            try {
                if(sen.aI(1, idventa).ejecutarSentencia() && sen.getRS().next()){
                    campoIdVenta.setText(sen.getRS().getString("venta.id"));
                    campoFechaVenta.setText( sen.getRS().getTimestamp("datetime").toString() );
                    campoMonto.setText( KFormat( sen.getRS().getInt("monto") )+ "Gs.");
                    campoVendedor.setText( Capitalize( sen.getRS().getString("usuario.nombre") ) );
                }
            } catch (SQLException ex) {
                Logger.getLogger(VerProductosVenta.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            sen.preparar("Select venta.id,detventa.cantidad,detventa.precioventa,coalesce(detpromo.nombredesc,producto.nombredesc) as nombreventa,venta.datetime from detventa join (Select id,caja_id,datetime from venta)as venta on venta.id = detventa.venta_id join producto on producto.id = detventa.producto_id left join detpromo on detpromo.detventa_id = detventa.id where venta_id = ?");
            if(sen.aI(1, idventa).ejecutarSentencia()){            
                try {
                    while(sen.getRS().next()){
                        Lista_Productos.add(new VentaProducto(sen.getRS().getInt("venta.id"),sen.getRS().getString("nombreventa"),sen.getRS().getInt("detventa.cantidad"),sen.getRS().getInt("detventa.precioventa"),sen.getRS().getTimestamp("datetime")));

                    }
                } catch (SQLException ex) {
                    Logger.getLogger(VerProductosVenta.class.getName()).log(Level.SEVERE, null, ex);
                }
            }        
        }
    }
    
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        Lista_Productos = observableArrayList();

        tablaVentas.setPlaceholder(new Label("No se ha realizado ninguna venta en el periodo de esta caja."));
        tablaVentas.getColumns().clear();
        tablaVentas.setItems(Lista_Productos);
        tablaVentas.getColumns().addAll(newCol("Nombre del producto","Nombre"),newCol("Cantidad vendida","CantidadVendida"),newCol("Precio Unitario","PrecioVenta"),newCol("Importe","Importe"));
    }
}
