/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FXML;

import FXML.secundarios.Caja;
import FXML.secundarios.GvarController;
import FXML.secundarios.Helper.tClass;
import static FXML.secundarios.Static.GenHelper.newCol;
import clases.SenPreparada;
import java.io.IOException;
import java.net.URL;
import java.sql.Date;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import static javafx.collections.FXCollections.observableArrayList;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TableView;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

/**
 *
 * @author Manabe
 */
public class VerCajasController extends GvarController{
    @FXML TableView<Caja> tablaCajas;
    @FXML DatePicker campoFecha;

    ObservableList<Caja> Lista_Cajas;
    
    @Override
    public void afterInit(){
                        
        defaultCajas();
    }
    
    @FXML void mostrarRegistroCaja() throws IOException{
        if(tablaCajas.getSelectionModel().getSelectedItem() != null){
            Caja caja = tablaCajas.getSelectionModel().getSelectedItem();
            Map<String,tClass> prms = new HashMap<>();
            prms.put( "id-caja", new tClass(caja.getId()) );
            
            Stage ventana = smanager.mostrarParam(smanager.VER_PRODUCTOS_CAJA, prms);
            if(ventana != null)
                ventana.showAndWait();
        
        }

    }
    
    @FXML void filtrarPorFecha(){
        SenPreparada sen = smanager.newSenPreparada();
        Date Fecha  = Date.valueOf(campoFecha.getValue());
        sen.preparar("Select * from caja where date(fechaabre) = ? ORDER BY ID DESC LIMIT 50");
        
        if(sen.aD(1,Fecha).ejecutarSentencia())
            try {
                Lista_Cajas.clear();
                while(sen.getRS().next()){
                    Lista_Cajas.add(new Caja(sen.getRS().getInt("id"),sen.getRS().getInt("abre"),sen.getRS().getInt("cierra"),sen.getRS().getTimestamp("fechaabre"),sen.getRS().getTimestamp("fechacierra")));
                }
        } catch (SQLException ex) {
            Logger.getLogger(VerCajasController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML void defaultCajas(){
        SenPreparada sen = smanager.newSenPreparada();
        sen.preparar("Select * from caja ORDER BY ID DESC LIMIT 50 ");
        
        if(sen.ejecutarSentencia())
            try {
                Lista_Cajas.clear();
                while(sen.getRS().next()){                    
                    Lista_Cajas.add(new Caja(sen.getRS().getInt("id"),sen.getRS().getInt("abre"),sen.getRS().getInt("cierra"),sen.getRS().getTimestamp("fechaabre"),sen.getRS().getTimestamp("fechacierra")));
                }
        } catch (SQLException ex) {
            Logger.getLogger(VerCajasController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        Lista_Cajas= observableArrayList();
        tablaCajas.setPlaceholder(new Label("No se han encontrado cajas."));
        tablaCajas.setItems(Lista_Cajas);

        
        tablaCajas.getColumns().clear();
        tablaCajas.getColumns().addAll(newCol("ID Caja","Id"),newCol("Monto apertura","AbreGs"),newCol("Monto cierre","CierraGs"),newCol("Fecha apertura","FechaAbre"),newCol("Fecha cierre","FechaCierra"));

    }
    
}
