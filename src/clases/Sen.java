/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package clases;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author 3rott
 */
public class Sen extends Conexion {//herencia
    ResultSet rs= null;//objeto para guardar los resultados
    Statement query;//objeto para guardar los resultados
    public Sen(){//constructor
        super();//constructor de la clase padre Conexion
    }
    ////realizar consultas a la base de datos 
    public ResultSet consultas(String sql){
        try {       
            query=conn.createStatement();
            rs = query.executeQuery(sql);
        } catch (SQLException ex) {
            Logger.getLogger(Sen.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return rs;
        }
    
        public void insertar(String sql){
            try {       
                query=conn.createStatement();
                query.executeUpdate(sql);
            } catch (SQLException ex) {
                Logger.getLogger(Sen.class.getName()).log(Level.SEVERE, null, ex);
            }        
        }
    
}
