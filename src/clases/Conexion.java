package clases;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author 3rott
 */
public class Conexion {
    public static Connection conn;
    private final static String bd="ferfran";
    private final static String login ="root";
    private final static String pass ="";
    private final static String host ="127.0.0.1";
    private final String url="jdbc:mysql://"+host+"/"+bd;
   
    public Conexion (){
        conexion();
    }
    
    public Connection get(){
        if(conn != null)
            return conn;
        else
            return null;
    }
    
    public boolean conexion (){
        try{//captura si hay algun error
            Class.forName("com.mysql.jdbc.Driver");//instaciamos
            conn=DriverManager.getConnection(url, login, pass);
            return true;
            
          
        }catch (Exception ex){//ex contiene el error si los hubiere
            JOptionPane.showMessageDialog(null,ex);//mostramos el 
            return false;
        }
    }

    public void cierraConexion(){
        try {                  
            conn.close();
        } catch (SQLException ex) {
            Logger.getLogger(Conexion.class.getName()).log(Level.SEVERE, null, ex);
        }
      
    }


}
