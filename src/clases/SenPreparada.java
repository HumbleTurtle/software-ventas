/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package clases;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 *
 * @author Manabe
 */
public class SenPreparada{
    private PreparedStatement e=null;
    public ResultSet res;
    boolean status;
    Conexion conn = null;
    
    public SenPreparada(Conexion conn){        
        super();
        status=true;
        this.conn = conn;        
    }  
    
    public boolean preparar(String s){        
        try {
            e = conn.get().prepareStatement(s);
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(SenPreparada.class.getName()).log(Level.SEVERE, null, ex);
           
        }
         return false;
    }

    public SenPreparada aS(int PARAMETRO,String VALOR){
        try {
            if(VALOR == null ){
                e.setNull(PARAMETRO, 1);
                return this;
            } 
            if(VALOR.isEmpty()) e.setNull(PARAMETRO, 1);
            else e.setString(PARAMETRO, VALOR);
        } catch (SQLException ex) {
            Logger.getLogger(SenPreparada.class.getName()).log(Level.SEVERE, null, ex);
        }
        return this;
    }
    
    public SenPreparada aB(int PARAMETRO, boolean b){
        try {
            e.setBoolean(PARAMETRO, b);
        } catch (SQLException ex) {
            Logger.getLogger(SenPreparada.class.getName()).log(Level.SEVERE, null, ex);
        }
        return this;
    }
    
    public SenPreparada aI(int PARAMETRO,int VALOR){
        try {
            e.setInt(PARAMETRO, (int) Double.parseDouble(VALOR+""));
        } catch (SQLException ex) {
            Logger.getLogger(SenPreparada.class.getName()).log(Level.SEVERE, null, ex);
        }
        return this;
    }
    
    public SenPreparada aI(int PARAMETRO,String VALOR){
        try {
            if(VALOR.isEmpty()) e.setNull(PARAMETRO, 1);
            else e.setInt(PARAMETRO, (int) Double.parseDouble(VALOR));
        } catch (SQLException ex) {
            Logger.getLogger(SenPreparada.class.getName()).log(Level.SEVERE, null, ex);
        }
        return this;
    }
    public SenPreparada aD(int PARAMETRO,Date date){
        try {
            e.setDate(PARAMETRO,date);
        } catch (SQLException ex) {
            Logger.getLogger(SenPreparada.class.getName()).log(Level.SEVERE, null, ex);
        }
        return this;
    }
    
    public boolean actualizar(){
        int rows= 0;
        try {
            if(status){
                 rows = e.executeUpdate();
                 res = e.getGeneratedKeys();
                 if(rows == 0)
                     return false;
                 else
                    return true;
            }
        } catch (SQLException ex) {
            Logger.getLogger(SenPreparada.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        return false;
        
    }
    public boolean ejecutarSentencia(){

        try {
            if(status){
                res = e.executeQuery();

                return true;
                
            }
                
            
        } catch (SQLException ex) {
            Logger.getLogger(SenPreparada.class.getName()).log(Level.SEVERE, null, ex);
            
        }
        return false;
    }
    
    public ResultSet getRS(){
        return res;
    }
    
    public ResultSet getGeneratedKeys() throws SQLException{
        return e.getGeneratedKeys();
    }
    
}
